import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './order.reducer';

export const OrderDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const orderEntity = useAppSelector(state => state.order.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="orderDetailsHeading">
          <Translate contentKey="bieShopApp.order.detail.title">Order</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{orderEntity.id}</dd>
          <dt>
            <span id="address">
              <Translate contentKey="bieShopApp.order.address">Address</Translate>
            </span>
          </dt>
          <dd>{orderEntity.address}</dd>
          <dt>
            <span id="phoneNumber">
              <Translate contentKey="bieShopApp.order.phoneNumber">Phone Number</Translate>
            </span>
          </dt>
          <dd>{orderEntity.phoneNumber}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="bieShopApp.order.name">Name</Translate>
            </span>
          </dt>
          <dd>{orderEntity.name}</dd>
          <dt>
            <span id="orderStatus">
              <Translate contentKey="bieShopApp.order.orderStatus">Order Status</Translate>
            </span>
          </dt>
          <dd>{orderEntity.orderStatus}</dd>
          <dt>
            <span id="orderAt">
              <Translate contentKey="bieShopApp.order.orderAt">Order At</Translate>
            </span>
          </dt>
          <dd>{orderEntity.orderAt ? <TextFormat value={orderEntity.orderAt} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="isDeleted">
              <Translate contentKey="bieShopApp.order.isDeleted">Is Deleted</Translate>
            </span>
          </dt>
          <dd>{orderEntity.isDeleted ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="bieShopApp.order.user">User</Translate>
          </dt>
          <dd>{orderEntity.user ? orderEntity.user.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/order" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/order/${orderEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default OrderDetail;
