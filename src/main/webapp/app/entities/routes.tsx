import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Category from './category';
import Product from './product';
import Inventory from './inventory';
import ProductImage from './product-image';
import Order from './order';
import OrderDetail from './order-detail';
import Discount from './discount';
import ProductOption from './product-option';
import Supplier from './supplier';
import InventoryTransactions from './inventory-transactions';
import Payment from './payment';
/* jhipster-needle-add-route-import - JHipster will add routes here */

export default () => {
  return (
    <div>
      <ErrorBoundaryRoutes>
        {/* prettier-ignore */}
        <Route path="category/*" element={<Category />} />
        <Route path="product/*" element={<Product />} />
        <Route path="inventory/*" element={<Inventory />} />
        <Route path="product-image/*" element={<ProductImage />} />
        <Route path="order/*" element={<Order />} />
        <Route path="order-detail/*" element={<OrderDetail />} />
        <Route path="discount/*" element={<Discount />} />
        <Route path="product-option/*" element={<ProductOption />} />
        <Route path="supplier/*" element={<Supplier />} />
        <Route path="inventory-transactions/*" element={<InventoryTransactions />} />
        <Route path="payment/*" element={<Payment />} />
        {/* jhipster-needle-add-route-path - JHipster will add routes here */}
      </ErrorBoundaryRoutes>
    </div>
  );
};
