import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './product-option.reducer';

export const ProductOptionDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const productOptionEntity = useAppSelector(state => state.productOption.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="productOptionDetailsHeading">
          <Translate contentKey="bieShopApp.productOption.detail.title">ProductOption</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{productOptionEntity.id}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="bieShopApp.productOption.name">Name</Translate>
            </span>
          </dt>
          <dd>{productOptionEntity.name}</dd>
          <dt>
            <span id="priceModifier">
              <Translate contentKey="bieShopApp.productOption.priceModifier">Price Modifier</Translate>
            </span>
          </dt>
          <dd>{productOptionEntity.priceModifier}</dd>
          <dt>
            <span id="unit">
              <Translate contentKey="bieShopApp.productOption.unit">Unit</Translate>
            </span>
          </dt>
          <dd>{productOptionEntity.unit}</dd>
          <dt>
            <span id="isDeleted">
              <Translate contentKey="bieShopApp.productOption.isDeleted">Is Deleted</Translate>
            </span>
          </dt>
          <dd>{productOptionEntity.isDeleted ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="bieShopApp.productOption.product">Product</Translate>
          </dt>
          <dd>{productOptionEntity.product ? productOptionEntity.product.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/product-option" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/product-option/${productOptionEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default ProductOptionDetail;
