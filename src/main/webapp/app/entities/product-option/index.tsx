import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import ProductOption from './product-option';
import ProductOptionDetail from './product-option-detail';
import ProductOptionUpdate from './product-option-update';
import ProductOptionDeleteDialog from './product-option-delete-dialog';

const ProductOptionRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<ProductOption />} />
    <Route path="new" element={<ProductOptionUpdate />} />
    <Route path=":id">
      <Route index element={<ProductOptionDetail />} />
      <Route path="edit" element={<ProductOptionUpdate />} />
      <Route path="delete" element={<ProductOptionDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default ProductOptionRoutes;
