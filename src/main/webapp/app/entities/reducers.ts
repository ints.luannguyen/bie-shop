import category from 'app/entities/category/category.reducer';
import product from 'app/entities/product/product.reducer';
import inventory from 'app/entities/inventory/inventory.reducer';
import productImage from 'app/entities/product-image/product-image.reducer';
import order from 'app/entities/order/order.reducer';
import orderDetail from 'app/entities/order-detail/order-detail.reducer';
import discount from 'app/entities/discount/discount.reducer';
import productOption from 'app/entities/product-option/product-option.reducer';
import supplier from 'app/entities/supplier/supplier.reducer';
import inventoryTransactions from 'app/entities/inventory-transactions/inventory-transactions.reducer';
import payment from 'app/entities/payment/payment.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

const entitiesReducers = {
  category,
  product,
  inventory,
  productImage,
  order,
  orderDetail,
  discount,
  productOption,
  supplier,
  inventoryTransactions,
  payment,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
};

export default entitiesReducers;
