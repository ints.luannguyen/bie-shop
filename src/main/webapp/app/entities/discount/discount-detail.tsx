import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './discount.reducer';

export const DiscountDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const discountEntity = useAppSelector(state => state.discount.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="discountDetailsHeading">
          <Translate contentKey="bieShopApp.discount.detail.title">Discount</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{discountEntity.id}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="bieShopApp.discount.name">Name</Translate>
            </span>
          </dt>
          <dd>{discountEntity.name}</dd>
          <dt>
            <span id="desc">
              <Translate contentKey="bieShopApp.discount.desc">Desc</Translate>
            </span>
          </dt>
          <dd>{discountEntity.desc}</dd>
          <dt>
            <span id="discountRate">
              <Translate contentKey="bieShopApp.discount.discountRate">Discount Rate</Translate>
            </span>
          </dt>
          <dd>{discountEntity.discountRate}</dd>
          <dt>
            <span id="startDate">
              <Translate contentKey="bieShopApp.discount.startDate">Start Date</Translate>
            </span>
          </dt>
          <dd>{discountEntity.startDate ? <TextFormat value={discountEntity.startDate} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="endDate">
              <Translate contentKey="bieShopApp.discount.endDate">End Date</Translate>
            </span>
          </dt>
          <dd>{discountEntity.endDate ? <TextFormat value={discountEntity.endDate} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="isDeleted">
              <Translate contentKey="bieShopApp.discount.isDeleted">Is Deleted</Translate>
            </span>
          </dt>
          <dd>{discountEntity.isDeleted ? 'true' : 'false'}</dd>
        </dl>
        <Button tag={Link} to="/discount" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/discount/${discountEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default DiscountDetail;
