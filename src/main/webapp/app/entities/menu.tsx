import React from 'react';
import { Translate } from 'react-jhipster';

import MenuItem from 'app/shared/layout/menus/menu-item';

const EntitiesMenu = () => {
  return (
    <>
      {/* prettier-ignore */}
      <MenuItem icon="asterisk" to="/category">
        <Translate contentKey="global.menu.entities.category" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/product">
        <Translate contentKey="global.menu.entities.product" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/inventory">
        <Translate contentKey="global.menu.entities.inventory" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/product-image">
        <Translate contentKey="global.menu.entities.productImage" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/order">
        <Translate contentKey="global.menu.entities.order" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/order-detail">
        <Translate contentKey="global.menu.entities.orderDetail" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/discount">
        <Translate contentKey="global.menu.entities.discount" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/product-option">
        <Translate contentKey="global.menu.entities.productOption" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/supplier">
        <Translate contentKey="global.menu.entities.supplier" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/inventory-transactions">
        <Translate contentKey="global.menu.entities.inventoryTransactions" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/payment">
        <Translate contentKey="global.menu.entities.payment" />
      </MenuItem>
      {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
    </>
  );
};

export default EntitiesMenu;
