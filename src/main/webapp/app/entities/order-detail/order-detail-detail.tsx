import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './order-detail.reducer';

export const OrderDetailDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const orderDetailEntity = useAppSelector(state => state.orderDetail.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="orderDetailDetailsHeading">
          <Translate contentKey="bieShopApp.orderDetail.detail.title">OrderDetail</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{orderDetailEntity.id}</dd>
          <dt>
            <span id="price">
              <Translate contentKey="bieShopApp.orderDetail.price">Price</Translate>
            </span>
          </dt>
          <dd>{orderDetailEntity.price}</dd>
          <dt>
            <span id="quantity">
              <Translate contentKey="bieShopApp.orderDetail.quantity">Quantity</Translate>
            </span>
          </dt>
          <dd>{orderDetailEntity.quantity}</dd>
          <dt>
            <span id="isDeleted">
              <Translate contentKey="bieShopApp.orderDetail.isDeleted">Is Deleted</Translate>
            </span>
          </dt>
          <dd>{orderDetailEntity.isDeleted ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="bieShopApp.orderDetail.order">Order</Translate>
          </dt>
          <dd>{orderDetailEntity.order ? orderDetailEntity.order.id : ''}</dd>
          <dt>
            <Translate contentKey="bieShopApp.orderDetail.product">Product</Translate>
          </dt>
          <dd>{orderDetailEntity.product ? orderDetailEntity.product.id : ''}</dd>
          <dt>
            <Translate contentKey="bieShopApp.orderDetail.productOption">Product Option</Translate>
          </dt>
          <dd>{orderDetailEntity.productOption ? orderDetailEntity.productOption.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/order-detail" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/order-detail/${orderDetailEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default OrderDetailDetail;
