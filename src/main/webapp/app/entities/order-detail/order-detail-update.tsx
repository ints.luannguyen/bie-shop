import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IOrder } from 'app/shared/model/order.model';
import { getEntities as getOrders } from 'app/entities/order/order.reducer';
import { IProduct } from 'app/shared/model/product.model';
import { getEntities as getProducts } from 'app/entities/product/product.reducer';
import { IProductOption } from 'app/shared/model/product-option.model';
import { getEntities as getProductOptions } from 'app/entities/product-option/product-option.reducer';
import { IOrderDetail } from 'app/shared/model/order-detail.model';
import { getEntity, updateEntity, createEntity, reset } from './order-detail.reducer';

export const OrderDetailUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const orders = useAppSelector(state => state.order.entities);
  const products = useAppSelector(state => state.product.entities);
  const productOptions = useAppSelector(state => state.productOption.entities);
  const orderDetailEntity = useAppSelector(state => state.orderDetail.entity);
  const loading = useAppSelector(state => state.orderDetail.loading);
  const updating = useAppSelector(state => state.orderDetail.updating);
  const updateSuccess = useAppSelector(state => state.orderDetail.updateSuccess);

  const handleClose = () => {
    navigate('/order-detail' + location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getOrders({}));
    dispatch(getProducts({}));
    dispatch(getProductOptions({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  // eslint-disable-next-line complexity
  const saveEntity = values => {
    if (values.id !== undefined && typeof values.id !== 'number') {
      values.id = Number(values.id);
    }
    if (values.price !== undefined && typeof values.price !== 'number') {
      values.price = Number(values.price);
    }
    if (values.quantity !== undefined && typeof values.quantity !== 'number') {
      values.quantity = Number(values.quantity);
    }

    const entity = {
      ...orderDetailEntity,
      ...values,
      order: orders.find(it => it.id.toString() === values.order?.toString()),
      product: products.find(it => it.id.toString() === values.product?.toString()),
      productOption: productOptions.find(it => it.id.toString() === values.productOption?.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...orderDetailEntity,
          order: orderDetailEntity?.order?.id,
          product: orderDetailEntity?.product?.id,
          productOption: orderDetailEntity?.productOption?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="bieShopApp.orderDetail.home.createOrEditLabel" data-cy="OrderDetailCreateUpdateHeading">
            <Translate contentKey="bieShopApp.orderDetail.home.createOrEditLabel">Create or edit a OrderDetail</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="order-detail-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('bieShopApp.orderDetail.price')}
                id="order-detail-price"
                name="price"
                data-cy="price"
                type="text"
              />
              <ValidatedField
                label={translate('bieShopApp.orderDetail.quantity')}
                id="order-detail-quantity"
                name="quantity"
                data-cy="quantity"
                type="text"
              />
              <ValidatedField
                label={translate('bieShopApp.orderDetail.isDeleted')}
                id="order-detail-isDeleted"
                name="isDeleted"
                data-cy="isDeleted"
                check
                type="checkbox"
              />
              <ValidatedField
                id="order-detail-order"
                name="order"
                data-cy="order"
                label={translate('bieShopApp.orderDetail.order')}
                type="select"
              >
                <option value="" key="0" />
                {orders
                  ? orders.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="order-detail-product"
                name="product"
                data-cy="product"
                label={translate('bieShopApp.orderDetail.product')}
                type="select"
              >
                <option value="" key="0" />
                {products
                  ? products.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="order-detail-productOption"
                name="productOption"
                data-cy="productOption"
                label={translate('bieShopApp.orderDetail.productOption')}
                type="select"
              >
                <option value="" key="0" />
                {productOptions
                  ? productOptions.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/order-detail" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default OrderDetailUpdate;
