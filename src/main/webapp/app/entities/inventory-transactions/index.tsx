import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import InventoryTransactions from './inventory-transactions';
import InventoryTransactionsDetail from './inventory-transactions-detail';
import InventoryTransactionsUpdate from './inventory-transactions-update';
import InventoryTransactionsDeleteDialog from './inventory-transactions-delete-dialog';

const InventoryTransactionsRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<InventoryTransactions />} />
    <Route path="new" element={<InventoryTransactionsUpdate />} />
    <Route path=":id">
      <Route index element={<InventoryTransactionsDetail />} />
      <Route path="edit" element={<InventoryTransactionsUpdate />} />
      <Route path="delete" element={<InventoryTransactionsDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default InventoryTransactionsRoutes;
