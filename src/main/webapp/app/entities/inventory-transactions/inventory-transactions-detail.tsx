import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, byteSize, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './inventory-transactions.reducer';

export const InventoryTransactionsDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const inventoryTransactionsEntity = useAppSelector(state => state.inventoryTransactions.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="inventoryTransactionsDetailsHeading">
          <Translate contentKey="bieShopApp.inventoryTransactions.detail.title">InventoryTransactions</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{inventoryTransactionsEntity.id}</dd>
          <dt>
            <span id="quantity">
              <Translate contentKey="bieShopApp.inventoryTransactions.quantity">Quantity</Translate>
            </span>
          </dt>
          <dd>{inventoryTransactionsEntity.quantity}</dd>
          <dt>
            <span id="unitPrice">
              <Translate contentKey="bieShopApp.inventoryTransactions.unitPrice">Unit Price</Translate>
            </span>
          </dt>
          <dd>{inventoryTransactionsEntity.unitPrice}</dd>
          <dt>
            <span id="transactionDate">
              <Translate contentKey="bieShopApp.inventoryTransactions.transactionDate">Transaction Date</Translate>
            </span>
          </dt>
          <dd>
            {inventoryTransactionsEntity.transactionDate ? (
              <TextFormat value={inventoryTransactionsEntity.transactionDate} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="note">
              <Translate contentKey="bieShopApp.inventoryTransactions.note">Note</Translate>
            </span>
          </dt>
          <dd>{inventoryTransactionsEntity.note}</dd>
          <dt>
            <span id="isDeleted">
              <Translate contentKey="bieShopApp.inventoryTransactions.isDeleted">Is Deleted</Translate>
            </span>
          </dt>
          <dd>{inventoryTransactionsEntity.isDeleted ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="bieShopApp.inventoryTransactions.user">User</Translate>
          </dt>
          <dd>{inventoryTransactionsEntity.user ? inventoryTransactionsEntity.user.id : ''}</dd>
          <dt>
            <Translate contentKey="bieShopApp.inventoryTransactions.supplier">Supplier</Translate>
          </dt>
          <dd>{inventoryTransactionsEntity.supplier ? inventoryTransactionsEntity.supplier.id : ''}</dd>
          <dt>
            <Translate contentKey="bieShopApp.inventoryTransactions.product">Product</Translate>
          </dt>
          <dd>{inventoryTransactionsEntity.product ? inventoryTransactionsEntity.product.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/inventory-transactions" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/inventory-transactions/${inventoryTransactionsEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default InventoryTransactionsDetail;
