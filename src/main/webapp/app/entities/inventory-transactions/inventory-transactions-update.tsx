import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { ISupplier } from 'app/shared/model/supplier.model';
import { getEntities as getSuppliers } from 'app/entities/supplier/supplier.reducer';
import { IProduct } from 'app/shared/model/product.model';
import { getEntities as getProducts } from 'app/entities/product/product.reducer';
import { IInventoryTransactions } from 'app/shared/model/inventory-transactions.model';
import { getEntity, updateEntity, createEntity, reset } from './inventory-transactions.reducer';

export const InventoryTransactionsUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const users = useAppSelector(state => state.userManagement.users);
  const suppliers = useAppSelector(state => state.supplier.entities);
  const products = useAppSelector(state => state.product.entities);
  const inventoryTransactionsEntity = useAppSelector(state => state.inventoryTransactions.entity);
  const loading = useAppSelector(state => state.inventoryTransactions.loading);
  const updating = useAppSelector(state => state.inventoryTransactions.updating);
  const updateSuccess = useAppSelector(state => state.inventoryTransactions.updateSuccess);

  const handleClose = () => {
    navigate('/inventory-transactions' + location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getUsers({}));
    dispatch(getSuppliers({}));
    dispatch(getProducts({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  // eslint-disable-next-line complexity
  const saveEntity = values => {
    if (values.id !== undefined && typeof values.id !== 'number') {
      values.id = Number(values.id);
    }
    if (values.quantity !== undefined && typeof values.quantity !== 'number') {
      values.quantity = Number(values.quantity);
    }
    if (values.unitPrice !== undefined && typeof values.unitPrice !== 'number') {
      values.unitPrice = Number(values.unitPrice);
    }
    values.transactionDate = convertDateTimeToServer(values.transactionDate);

    const entity = {
      ...inventoryTransactionsEntity,
      ...values,
      user: users.find(it => it.id.toString() === values.user?.toString()),
      supplier: suppliers.find(it => it.id.toString() === values.supplier?.toString()),
      product: products.find(it => it.id.toString() === values.product?.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          transactionDate: displayDefaultDateTime(),
        }
      : {
          ...inventoryTransactionsEntity,
          transactionDate: convertDateTimeFromServer(inventoryTransactionsEntity.transactionDate),
          user: inventoryTransactionsEntity?.user?.id,
          supplier: inventoryTransactionsEntity?.supplier?.id,
          product: inventoryTransactionsEntity?.product?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="bieShopApp.inventoryTransactions.home.createOrEditLabel" data-cy="InventoryTransactionsCreateUpdateHeading">
            <Translate contentKey="bieShopApp.inventoryTransactions.home.createOrEditLabel">
              Create or edit a InventoryTransactions
            </Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="inventory-transactions-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('bieShopApp.inventoryTransactions.quantity')}
                id="inventory-transactions-quantity"
                name="quantity"
                data-cy="quantity"
                type="text"
              />
              <ValidatedField
                label={translate('bieShopApp.inventoryTransactions.unitPrice')}
                id="inventory-transactions-unitPrice"
                name="unitPrice"
                data-cy="unitPrice"
                type="text"
              />
              <ValidatedField
                label={translate('bieShopApp.inventoryTransactions.transactionDate')}
                id="inventory-transactions-transactionDate"
                name="transactionDate"
                data-cy="transactionDate"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('bieShopApp.inventoryTransactions.note')}
                id="inventory-transactions-note"
                name="note"
                data-cy="note"
                type="textarea"
              />
              <ValidatedField
                label={translate('bieShopApp.inventoryTransactions.isDeleted')}
                id="inventory-transactions-isDeleted"
                name="isDeleted"
                data-cy="isDeleted"
                check
                type="checkbox"
              />
              <ValidatedField
                id="inventory-transactions-user"
                name="user"
                data-cy="user"
                label={translate('bieShopApp.inventoryTransactions.user')}
                type="select"
              >
                <option value="" key="0" />
                {users
                  ? users.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="inventory-transactions-supplier"
                name="supplier"
                data-cy="supplier"
                label={translate('bieShopApp.inventoryTransactions.supplier')}
                type="select"
              >
                <option value="" key="0" />
                {suppliers
                  ? suppliers.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="inventory-transactions-product"
                name="product"
                data-cy="product"
                label={translate('bieShopApp.inventoryTransactions.product')}
                type="select"
              >
                <option value="" key="0" />
                {products
                  ? products.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/inventory-transactions" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default InventoryTransactionsUpdate;
