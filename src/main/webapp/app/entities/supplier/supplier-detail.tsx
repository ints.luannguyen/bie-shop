import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './supplier.reducer';

export const SupplierDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const supplierEntity = useAppSelector(state => state.supplier.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="supplierDetailsHeading">
          <Translate contentKey="bieShopApp.supplier.detail.title">Supplier</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{supplierEntity.id}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="bieShopApp.supplier.name">Name</Translate>
            </span>
          </dt>
          <dd>{supplierEntity.name}</dd>
          <dt>
            <span id="address">
              <Translate contentKey="bieShopApp.supplier.address">Address</Translate>
            </span>
          </dt>
          <dd>{supplierEntity.address}</dd>
          <dt>
            <span id="email">
              <Translate contentKey="bieShopApp.supplier.email">Email</Translate>
            </span>
          </dt>
          <dd>{supplierEntity.email}</dd>
          <dt>
            <span id="phone">
              <Translate contentKey="bieShopApp.supplier.phone">Phone</Translate>
            </span>
          </dt>
          <dd>{supplierEntity.phone}</dd>
          <dt>
            <span id="isDeleted">
              <Translate contentKey="bieShopApp.supplier.isDeleted">Is Deleted</Translate>
            </span>
          </dt>
          <dd>{supplierEntity.isDeleted ? 'true' : 'false'}</dd>
        </dl>
        <Button tag={Link} to="/supplier" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/supplier/${supplierEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default SupplierDetail;
