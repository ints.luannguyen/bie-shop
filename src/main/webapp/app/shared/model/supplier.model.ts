export interface ISupplier {
  id?: number;
  name?: string;
  address?: string;
  email?: string;
  phone?: string | null;
  isDeleted?: boolean | null;
}

export const defaultValue: Readonly<ISupplier> = {
  isDeleted: false,
};
