export interface ICategory {
  id?: number;
  name?: string;
  desc?: string | null;
  isDeleted?: boolean | null;
  parent?: ICategory | null;
}

export const defaultValue: Readonly<ICategory> = {
  isDeleted: false,
};
