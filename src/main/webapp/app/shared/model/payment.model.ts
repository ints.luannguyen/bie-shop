import dayjs from 'dayjs';
import { IOrder } from 'app/shared/model/order.model';
import { PaymentStatus } from 'app/shared/model/enumerations/payment-status.model';

export interface IPayment {
  id?: number;
  paymentMethod?: string | null;
  amount?: number | null;
  paymentDate?: dayjs.Dayjs | null;
  status?: keyof typeof PaymentStatus | null;
  isDeleted?: boolean | null;
  order?: IOrder | null;
}

export const defaultValue: Readonly<IPayment> = {
  isDeleted: false,
};
