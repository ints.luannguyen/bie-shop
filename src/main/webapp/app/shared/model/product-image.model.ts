import { IProduct } from 'app/shared/model/product.model';

export interface IProductImage {
  id?: number;
  image?: string | null;
  isDeleted?: boolean | null;
  product?: IProduct | null;
}

export const defaultValue: Readonly<IProductImage> = {
  isDeleted: false,
};
