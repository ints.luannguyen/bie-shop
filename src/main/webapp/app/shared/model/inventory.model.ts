import dayjs from 'dayjs';
import { IProduct } from 'app/shared/model/product.model';

export interface IInventory {
  id?: number;
  expireDate?: dayjs.Dayjs | null;
  quantity?: number | null;
  isDeleted?: boolean | null;
  product?: IProduct | null;
}

export const defaultValue: Readonly<IInventory> = {
  isDeleted: false,
};
