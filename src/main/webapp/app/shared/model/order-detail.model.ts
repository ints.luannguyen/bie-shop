import { IOrder } from 'app/shared/model/order.model';
import { IProduct } from 'app/shared/model/product.model';
import { IProductOption } from 'app/shared/model/product-option.model';

export interface IOrderDetail {
  id?: number;
  price?: number | null;
  quantity?: number | null;
  isDeleted?: boolean | null;
  order?: IOrder | null;
  product?: IProduct | null;
  productOption?: IProductOption | null;
}

export const defaultValue: Readonly<IOrderDetail> = {
  isDeleted: false,
};
