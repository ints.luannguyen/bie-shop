import dayjs from 'dayjs';

export interface IDiscount {
  id?: number;
  name?: string;
  desc?: string | null;
  discountRate?: number;
  startDate?: dayjs.Dayjs;
  endDate?: dayjs.Dayjs;
  isDeleted?: boolean | null;
}

export const defaultValue: Readonly<IDiscount> = {
  isDeleted: false,
};
