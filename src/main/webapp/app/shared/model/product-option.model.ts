import { IProduct } from 'app/shared/model/product.model';

export interface IProductOption {
  id?: number;
  name?: string;
  priceModifier?: number;
  unit?: string;
  isDeleted?: boolean | null;
  product?: IProduct | null;
}

export const defaultValue: Readonly<IProductOption> = {
  isDeleted: false,
};
