import dayjs from 'dayjs';
import { IUser } from 'app/shared/model/user.model';
import { OrderStatus } from 'app/shared/model/enumerations/order-status.model';

export interface IOrder {
  id?: number;
  address?: string;
  phoneNumber?: string;
  name?: string;
  orderStatus?: keyof typeof OrderStatus | null;
  orderAt?: dayjs.Dayjs | null;
  isDeleted?: boolean | null;
  user?: IUser | null;
}

export const defaultValue: Readonly<IOrder> = {
  isDeleted: false,
};
