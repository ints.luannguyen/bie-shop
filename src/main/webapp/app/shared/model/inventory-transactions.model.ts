import dayjs from 'dayjs';
import { IUser } from 'app/shared/model/user.model';
import { ISupplier } from 'app/shared/model/supplier.model';
import { IProduct } from 'app/shared/model/product.model';

export interface IInventoryTransactions {
  id?: number;
  quantity?: number | null;
  unitPrice?: number | null;
  transactionDate?: dayjs.Dayjs | null;
  note?: string | null;
  isDeleted?: boolean | null;
  user?: IUser | null;
  supplier?: ISupplier | null;
  product?: IProduct | null;
}

export const defaultValue: Readonly<IInventoryTransactions> = {
  isDeleted: false,
};
