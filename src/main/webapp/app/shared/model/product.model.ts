import { ICategory } from 'app/shared/model/category.model';
import { IDiscount } from 'app/shared/model/discount.model';
import { ISupplier } from 'app/shared/model/supplier.model';

export interface IProduct {
  id?: number;
  name?: string;
  desc?: string | null;
  thumbnail?: string | null;
  price?: number;
  isDeleted?: boolean | null;
  category?: ICategory | null;
  discount?: IDiscount | null;
  supplier?: ISupplier | null;
}

export const defaultValue: Readonly<IProduct> = {
  isDeleted: false,
};
