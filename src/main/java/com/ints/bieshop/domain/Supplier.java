package com.ints.bieshop.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Supplier.
 */
@Entity
@Table(name = "supplier")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Supplier implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "address", nullable = false)
    private String address;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "supplier")
    @JsonIgnoreProperties(
        value = {
            "orderDetails", "productImages", "productOptions", "inventoryTransactions", "inventories", "category", "discount", "supplier",
        },
        allowSetters = true
    )
    private Set<Product> products = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "supplier")
    @JsonIgnoreProperties(value = { "user", "supplier", "product" }, allowSetters = true)
    private Set<InventoryTransactions> inventoryTransactions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Supplier id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Supplier name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return this.address;
    }

    public Supplier address(String address) {
        this.setAddress(address);
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return this.email;
    }

    public Supplier email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return this.phone;
    }

    public Supplier phone(String phone) {
        this.setPhone(phone);
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getIsDeleted() {
        return this.isDeleted;
    }

    public Supplier isDeleted(Boolean isDeleted) {
        this.setIsDeleted(isDeleted);
        return this;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Set<Product> getProducts() {
        return this.products;
    }

    public void setProducts(Set<Product> products) {
        if (this.products != null) {
            this.products.forEach(i -> i.setSupplier(null));
        }
        if (products != null) {
            products.forEach(i -> i.setSupplier(this));
        }
        this.products = products;
    }

    public Supplier products(Set<Product> products) {
        this.setProducts(products);
        return this;
    }

    public Supplier addProduct(Product product) {
        this.products.add(product);
        product.setSupplier(this);
        return this;
    }

    public Supplier removeProduct(Product product) {
        this.products.remove(product);
        product.setSupplier(null);
        return this;
    }

    public Set<InventoryTransactions> getInventoryTransactions() {
        return this.inventoryTransactions;
    }

    public void setInventoryTransactions(Set<InventoryTransactions> inventoryTransactions) {
        if (this.inventoryTransactions != null) {
            this.inventoryTransactions.forEach(i -> i.setSupplier(null));
        }
        if (inventoryTransactions != null) {
            inventoryTransactions.forEach(i -> i.setSupplier(this));
        }
        this.inventoryTransactions = inventoryTransactions;
    }

    public Supplier inventoryTransactions(Set<InventoryTransactions> inventoryTransactions) {
        this.setInventoryTransactions(inventoryTransactions);
        return this;
    }

    public Supplier addInventoryTransactions(InventoryTransactions inventoryTransactions) {
        this.inventoryTransactions.add(inventoryTransactions);
        inventoryTransactions.setSupplier(this);
        return this;
    }

    public Supplier removeInventoryTransactions(InventoryTransactions inventoryTransactions) {
        this.inventoryTransactions.remove(inventoryTransactions);
        inventoryTransactions.setSupplier(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Supplier)) {
            return false;
        }
        return getId() != null && getId().equals(((Supplier) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Supplier{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", address='" + getAddress() + "'" +
            ", email='" + getEmail() + "'" +
            ", phone='" + getPhone() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            "}";
    }
}
