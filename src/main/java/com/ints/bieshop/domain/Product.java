package com.ints.bieshop.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Product.
 */
@Entity
@Table(name = "product")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "jhi_desc")
    private String desc;

    @Column(name = "thumbnail")
    private String thumbnail;

    @NotNull
    @Column(name = "price", nullable = false)
    private Double price;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "product")
    @JsonIgnoreProperties(value = { "order", "product", "productOption" }, allowSetters = true)
    private Set<OrderDetail> orderDetails = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "product")
    @JsonIgnoreProperties(value = { "product" }, allowSetters = true)
    private Set<ProductImage> productImages = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "product")
    @JsonIgnoreProperties(value = { "orderDetails", "product" }, allowSetters = true)
    private Set<ProductOption> productOptions = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "product")
    @JsonIgnoreProperties(value = { "user", "supplier", "product" }, allowSetters = true)
    private Set<InventoryTransactions> inventoryTransactions = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "product")
    @JsonIgnoreProperties(value = { "product" }, allowSetters = true)
    private Set<Inventory> inventories = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties(value = { "products", "categories", "parent" }, allowSetters = true)
    private Category category;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties(value = { "products" }, allowSetters = true)
    private Discount discount;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties(value = { "products", "inventoryTransactions" }, allowSetters = true)
    private Supplier supplier;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Product id(Long id) {
        this.setId(id);
        return this;
    }

    public Product setId(Long id) {
        this.id = id;
        return null;
    }

    public String getName() {
        return this.name;
    }

    public Product name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return this.desc;
    }

    public Product desc(String desc) {
        this.setDesc(desc);
        return this;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getThumbnail() {
        return this.thumbnail;
    }

    public Product thumbnail(String thumbnail) {
        this.setThumbnail(thumbnail);
        return this;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Double getPrice() {
        return this.price;
    }

    public Product price(Double price) {
        this.setPrice(price);
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getIsDeleted() {
        return this.isDeleted;
    }

    public Product isDeleted(Boolean isDeleted) {
        this.setIsDeleted(isDeleted);
        return this;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Set<OrderDetail> getOrderDetails() {
        return this.orderDetails;
    }

    public void setOrderDetails(Set<OrderDetail> orderDetails) {
        if (this.orderDetails != null) {
            this.orderDetails.forEach(i -> i.setProduct(null));
        }
        if (orderDetails != null) {
            orderDetails.forEach(i -> i.setProduct(this));
        }
        this.orderDetails = orderDetails;
    }

    public Product orderDetails(Set<OrderDetail> orderDetails) {
        this.setOrderDetails(orderDetails);
        return this;
    }

    public Product addOrderDetail(OrderDetail orderDetail) {
        this.orderDetails.add(orderDetail);
        orderDetail.setProduct(this);
        return this;
    }

    public Product removeOrderDetail(OrderDetail orderDetail) {
        this.orderDetails.remove(orderDetail);
        orderDetail.setProduct(null);
        return this;
    }

    public Set<ProductImage> getProductImages() {
        return this.productImages;
    }

    public void setProductImages(Set<ProductImage> productImages) {
        if (this.productImages != null) {
            this.productImages.forEach(i -> i.setProduct(null));
        }
        if (productImages != null) {
            productImages.forEach(i -> i.setProduct(this));
        }
        this.productImages = productImages;
    }

    public Product productImages(Set<ProductImage> productImages) {
        this.setProductImages(productImages);
        return this;
    }

    public Product addProductImage(ProductImage productImage) {
        this.productImages.add(productImage);
        productImage.setProduct(this);
        return this;
    }

    public Product removeProductImage(ProductImage productImage) {
        this.productImages.remove(productImage);
        productImage.setProduct(null);
        return this;
    }

    public Set<ProductOption> getProductOptions() {
        return this.productOptions;
    }

    public void setProductOptions(Set<ProductOption> productOptions) {
        if (this.productOptions != null) {
            this.productOptions.forEach(i -> i.setProduct(null));
        }
        if (productOptions != null) {
            productOptions.forEach(i -> i.setProduct(this));
        }
        this.productOptions = productOptions;
    }

    public Product productOptions(Set<ProductOption> productOptions) {
        this.setProductOptions(productOptions);
        return this;
    }

    public Product addProductOption(ProductOption productOption) {
        this.productOptions.add(productOption);
        productOption.setProduct(this);
        return this;
    }

    public Product removeProductOption(ProductOption productOption) {
        this.productOptions.remove(productOption);
        productOption.setProduct(null);
        return this;
    }

    public Set<InventoryTransactions> getInventoryTransactions() {
        return this.inventoryTransactions;
    }

    public void setInventoryTransactions(Set<InventoryTransactions> inventoryTransactions) {
        if (this.inventoryTransactions != null) {
            this.inventoryTransactions.forEach(i -> i.setProduct(null));
        }
        if (inventoryTransactions != null) {
            inventoryTransactions.forEach(i -> i.setProduct(this));
        }
        this.inventoryTransactions = inventoryTransactions;
    }

    public Product inventoryTransactions(Set<InventoryTransactions> inventoryTransactions) {
        this.setInventoryTransactions(inventoryTransactions);
        return this;
    }

    public Product addInventoryTransactions(InventoryTransactions inventoryTransactions) {
        this.inventoryTransactions.add(inventoryTransactions);
        inventoryTransactions.setProduct(this);
        return this;
    }

    public Product removeInventoryTransactions(InventoryTransactions inventoryTransactions) {
        this.inventoryTransactions.remove(inventoryTransactions);
        inventoryTransactions.setProduct(null);
        return this;
    }

    public Set<Inventory> getInventories() {
        return this.inventories;
    }

    public void setInventories(Set<Inventory> inventories) {
        if (this.inventories != null) {
            this.inventories.forEach(i -> i.setProduct(null));
        }
        if (inventories != null) {
            inventories.forEach(i -> i.setProduct(this));
        }
        this.inventories = inventories;
    }

    public Product inventories(Set<Inventory> inventories) {
        this.setInventories(inventories);
        return this;
    }

    public Product addInventory(Inventory inventory) {
        this.inventories.add(inventory);
        inventory.setProduct(this);
        return this;
    }

    public Product removeInventory(Inventory inventory) {
        this.inventories.remove(inventory);
        inventory.setProduct(null);
        return this;
    }

    public Category getCategory() {
        return this.category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Product category(Category category) {
        this.setCategory(category);
        return this;
    }

    public Discount getDiscount() {
        return this.discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public Product discount(Discount discount) {
        this.setDiscount(discount);
        return this;
    }

    public Supplier getSupplier() {
        return this.supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Product supplier(Supplier supplier) {
        this.setSupplier(supplier);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Product)) {
            return false;
        }
        return getId() != null && getId().equals(((Product) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Product{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", desc='" + getDesc() + "'" +
            ", thumbnail='" + getThumbnail() + "'" +
            ", price=" + getPrice() +
            ", isDeleted='" + getIsDeleted() + "'" +
            "}";
    }
}
