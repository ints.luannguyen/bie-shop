package com.ints.bieshop.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A ProductOption.
 */
@Entity
@Table(name = "product_option")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ProductOption implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "price_modifier", nullable = false)
    private Double priceModifier;

    @NotNull
    @Column(name = "unit", nullable = false)
    private String unit;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "productOption")
    @JsonIgnoreProperties(value = { "order", "product", "productOption" }, allowSetters = true)
    private Set<OrderDetail> orderDetails = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(
        value = {
            "orderDetails", "productImages", "productOptions", "inventoryTransactions", "inventories", "category", "discount", "supplier",
        },
        allowSetters = true
    )
    private Product product;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ProductOption id(Long id) {
        this.setId(id);
        return this;
    }

    public ProductOption setId(Long id) {
        this.id = id;
        return null;
    }

    public String getName() {
        return this.name;
    }

    public ProductOption name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPriceModifier() {
        return this.priceModifier;
    }

    public ProductOption priceModifier(Double priceModifier) {
        this.setPriceModifier(priceModifier);
        return this;
    }

    public void setPriceModifier(Double priceModifier) {
        this.priceModifier = priceModifier;
    }

    public String getUnit() {
        return this.unit;
    }

    public ProductOption unit(String unit) {
        this.setUnit(unit);
        return this;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Boolean getIsDeleted() {
        return this.isDeleted;
    }

    public ProductOption isDeleted(Boolean isDeleted) {
        this.setIsDeleted(isDeleted);
        return this;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Set<OrderDetail> getOrderDetails() {
        return this.orderDetails;
    }

    public void setOrderDetails(Set<OrderDetail> orderDetails) {
        if (this.orderDetails != null) {
            this.orderDetails.forEach(i -> i.setProductOption(null));
        }
        if (orderDetails != null) {
            orderDetails.forEach(i -> i.setProductOption(this));
        }
        this.orderDetails = orderDetails;
    }

    public ProductOption orderDetails(Set<OrderDetail> orderDetails) {
        this.setOrderDetails(orderDetails);
        return this;
    }

    public ProductOption addOrderDetail(OrderDetail orderDetail) {
        this.orderDetails.add(orderDetail);
        orderDetail.setProductOption(this);
        return this;
    }

    public ProductOption removeOrderDetail(OrderDetail orderDetail) {
        this.orderDetails.remove(orderDetail);
        orderDetail.setProductOption(null);
        return this;
    }

    public Product getProduct() {
        return this.product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ProductOption product(Product product) {
        this.setProduct(product);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductOption)) {
            return false;
        }
        return getId() != null && getId().equals(((ProductOption) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductOption{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", priceModifier=" + getPriceModifier() +
            ", unit='" + getUnit() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            "}";
    }
}
