package com.ints.bieshop.domain.enumeration;

/**
 * The PaymentStatus enumeration.
 */
public enum PaymentStatus {
    SUCCESS,
    FAILURE,
}
