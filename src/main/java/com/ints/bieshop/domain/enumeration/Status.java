package com.ints.bieshop.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    PENDING,
    PROCESSING,
    SHIPPED,
    DELIVERED,
    CANCELLED,
}
