package com.ints.bieshop.service;

import com.ints.bieshop.domain.Discount;
import com.ints.bieshop.repository.DiscountRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.ints.bieshop.domain.Discount}.
 */
@Service
@Transactional
public class DiscountService {

    private final Logger log = LoggerFactory.getLogger(DiscountService.class);

    private final DiscountRepository discountRepository;

    public DiscountService(DiscountRepository discountRepository) {
        this.discountRepository = discountRepository;
    }

    /**
     * Save a discount.
     *
     * @param discount the entity to save.
     * @return the persisted entity.
     */
    public Discount save(Discount discount) {
        log.debug("Request to save Discount : {}", discount);
        return discountRepository.save(discount);
    }

    /**
     * Update a discount.
     *
     * @param discount the entity to save.
     * @return the persisted entity.
     */
    public Discount update(Discount discount) {
        log.debug("Request to update Discount : {}", discount);
        return discountRepository.save(discount);
    }

    /**
     * Partially update a discount.
     *
     * @param discount the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Discount> partialUpdate(Discount discount) {
        log.debug("Request to partially update Discount : {}", discount);

        return discountRepository
            .findById(discount.getId())
            .map(existingDiscount -> {
                if (discount.getName() != null) {
                    existingDiscount.setName(discount.getName());
                }
                if (discount.getDesc() != null) {
                    existingDiscount.setDesc(discount.getDesc());
                }
                if (discount.getDiscountRate() != null) {
                    existingDiscount.setDiscountRate(discount.getDiscountRate());
                }
                if (discount.getStartDate() != null) {
                    existingDiscount.setStartDate(discount.getStartDate());
                }
                if (discount.getEndDate() != null) {
                    existingDiscount.setEndDate(discount.getEndDate());
                }
                if (discount.getIsDeleted() != null) {
                    existingDiscount.setIsDeleted(discount.getIsDeleted());
                }

                return existingDiscount;
            })
            .map(discountRepository::save);
    }

    /**
     * Get all the discounts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Discount> findAll(Pageable pageable) {
        log.debug("Request to get all Discounts");
        return discountRepository.findAll(pageable);
    }

    /**
     * Get one discount by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Discount> findOne(Long id) {
        log.debug("Request to get Discount : {}", id);
        return discountRepository.findById(id);
    }

    /**
     * Delete the discount by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Discount : {}", id);
        discountRepository.deleteById(id);
    }
}
