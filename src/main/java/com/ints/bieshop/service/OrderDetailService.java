package com.ints.bieshop.service;

import com.ints.bieshop.domain.OrderDetail;
import com.ints.bieshop.repository.OrderDetailRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.ints.bieshop.domain.OrderDetail}.
 */
@Service
@Transactional
public class OrderDetailService {

    private final Logger log = LoggerFactory.getLogger(OrderDetailService.class);

    private final OrderDetailRepository orderDetailRepository;

    public OrderDetailService(OrderDetailRepository orderDetailRepository) {
        this.orderDetailRepository = orderDetailRepository;
    }

    /**
     * Save a orderDetail.
     *
     * @param orderDetail the entity to save.
     * @return the persisted entity.
     */
    public OrderDetail save(OrderDetail orderDetail) {
        log.debug("Request to save OrderDetail : {}", orderDetail);
        return orderDetailRepository.save(orderDetail);
    }

    /**
     * Update a orderDetail.
     *
     * @param orderDetail the entity to save.
     * @return the persisted entity.
     */
    public OrderDetail update(OrderDetail orderDetail) {
        log.debug("Request to update OrderDetail : {}", orderDetail);
        return orderDetailRepository.save(orderDetail);
    }

    /**
     * Partially update a orderDetail.
     *
     * @param orderDetail the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<OrderDetail> partialUpdate(OrderDetail orderDetail) {
        log.debug("Request to partially update OrderDetail : {}", orderDetail);

        return orderDetailRepository
            .findById(orderDetail.getId())
            .map(existingOrderDetail -> {
                if (orderDetail.getPrice() != null) {
                    existingOrderDetail.setPrice(orderDetail.getPrice());
                }
                if (orderDetail.getQuantity() != null) {
                    existingOrderDetail.setQuantity(orderDetail.getQuantity());
                }
                if (orderDetail.getIsDeleted() != null) {
                    existingOrderDetail.setIsDeleted(orderDetail.getIsDeleted());
                }

                return existingOrderDetail;
            })
            .map(orderDetailRepository::save);
    }

    /**
     * Get all the orderDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<OrderDetail> findAll(Pageable pageable) {
        log.debug("Request to get all OrderDetails");
        return orderDetailRepository.findAll(pageable);
    }

    /**
     * Get one orderDetail by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OrderDetail> findOne(Long id) {
        log.debug("Request to get OrderDetail : {}", id);
        return orderDetailRepository.findById(id);
    }

    /**
     * Delete the orderDetail by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OrderDetail : {}", id);
        orderDetailRepository.deleteById(id);
    }
}
