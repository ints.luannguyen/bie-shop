package com.ints.bieshop.service;

import com.ints.bieshop.domain.ProductImage;
import com.ints.bieshop.repository.ProductImageRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.ints.bieshop.domain.ProductImage}.
 */
@Service
@Transactional
public class ProductImageService {

    private final Logger log = LoggerFactory.getLogger(ProductImageService.class);

    private final ProductImageRepository productImageRepository;

    public ProductImageService(ProductImageRepository productImageRepository) {
        this.productImageRepository = productImageRepository;
    }

    /**
     * Save a productImage.
     *
     * @param productImage the entity to save.
     * @return the persisted entity.
     */
    public ProductImage save(ProductImage productImage) {
        log.debug("Request to save ProductImage : {}", productImage);
        return productImageRepository.save(productImage);
    }

    /**
     * Update a productImage.
     *
     * @param productImage the entity to save.
     * @return the persisted entity.
     */
    public ProductImage update(ProductImage productImage) {
        log.debug("Request to update ProductImage : {}", productImage);
        return productImageRepository.save(productImage);
    }

    /**
     * Partially update a productImage.
     *
     * @param productImage the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ProductImage> partialUpdate(ProductImage productImage) {
        log.debug("Request to partially update ProductImage : {}", productImage);

        return productImageRepository
            .findById(productImage.getId())
            .map(existingProductImage -> {
                if (productImage.getImage() != null) {
                    existingProductImage.setImage(productImage.getImage());
                }
                if (productImage.getIsDeleted() != null) {
                    existingProductImage.setIsDeleted(productImage.getIsDeleted());
                }

                return existingProductImage;
            })
            .map(productImageRepository::save);
    }

    /**
     * Get all the productImages.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ProductImage> findAll(Pageable pageable) {
        log.debug("Request to get all ProductImages");
        return productImageRepository.findAll(pageable);
    }

    /**
     * Get one productImage by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ProductImage> findOne(Long id) {
        log.debug("Request to get ProductImage : {}", id);
        return productImageRepository.findById(id);
    }

    /**
     * Delete the productImage by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ProductImage : {}", id);
        productImageRepository.deleteById(id);
    }
}
