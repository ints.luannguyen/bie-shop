package com.ints.bieshop.service.mapper;

import com.ints.bieshop.domain.*;
import com.ints.bieshop.service.dto.DiscountDTO;
import com.ints.bieshop.service.dto.InventoryDTO;
import com.ints.bieshop.service.dto.InventoryTransactionDTO;
import com.ints.bieshop.service.dto.ProductDTO;
import java.time.ZoneOffset;

public class DTOToModelMapper {

    private DTOToModelMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static Inventory dtoToEntity(Inventory inventory, InventoryDTO inventoryDTO, Product product) {
        inventory.setExpireDate(inventoryDTO.getExpireDate().toInstant(ZoneOffset.UTC));
        inventory.setQuantity(inventoryDTO.getQuantity());
        inventory.setIsDeleted(false);
        inventory.setProduct(product);
        return inventory;
    }

    public static Discount dtoToEntity(DiscountDTO discountDTO, Discount discount) {
        discount.setName(discountDTO.getName());
        discount.setDesc(discountDTO.getDesc());
        discount.setIsDeleted(false);
        discount.setDiscountRate(discountDTO.getDiscountRate());
        discount.setEndDate(discountDTO.getEndDate().toInstant(ZoneOffset.UTC));
        discount.setStartDate(discountDTO.getStartDate().toInstant(ZoneOffset.UTC));
        return discount;
    }

    public static Product dtoToEntity(Product product, ProductDTO productDTO, Category category, Supplier supplier, Discount discount) {
        product.setCategory(category);
        product.setSupplier(supplier);
        product.setDiscount(discount);
        product.setIsDeleted(false);
        product.setPrice(productDTO.getPrice());
        product.setName(productDTO.getName());
        product.setDesc(productDTO.getDesc());
        return product;
    }

    public static InventoryTransactions dtoToEntity(
        Product product,
        Supplier supplier,
        InventoryTransactionDTO inventoryTransactionDTO,
        InventoryTransactions inventoryTransactions,
        User user
    ) {
        inventoryTransactions.setProduct(product);
        inventoryTransactions.setSupplier(supplier);
        inventoryTransactions.setQuantity(inventoryTransactionDTO.getQuantity());
        inventoryTransactions.setIsDeleted(false);
        inventoryTransactions.setUser(user);
        inventoryTransactions.setTransactionDate(inventoryTransactionDTO.getTransactionDate().toInstant(ZoneOffset.UTC));
        inventoryTransactions.setQuantity(inventoryTransactionDTO.getQuantity());
        inventoryTransactions.setUnitPrice(inventoryTransactionDTO.getUnitPrice());
        inventoryTransactions.setNote(inventoryTransactionDTO.getNote());
        return inventoryTransactions;
    }
}
