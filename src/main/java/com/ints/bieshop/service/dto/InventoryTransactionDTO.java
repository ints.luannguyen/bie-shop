package com.ints.bieshop.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;
import lombok.*;

//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//@Getter
//@Setter
//@Builder
public class InventoryTransactionDTO {

    @Min(value = 0, message = "Price of product must be >= 0")
    @JsonProperty("unit_price")
    private float unitPrice;

    @Min(value = 1, message = "Number of products must be >= 1")
    @JsonProperty("quantity")
    private int quantity;

    @NotNull(message = "transaction_date is not null")
    @JsonProperty("transaction_date")
    private LocalDateTime transactionDate;

    @NotBlank(message = "Notes is not blank")
    @JsonProperty("note")
    private String note;

    @Min(value = 1, message = "Supplier ID must be >= 1 and not null")
    @JsonProperty("supplier_id")
    private long supplierId;

    @Min(value = 1, message = "Product ID must be >= 1 and not null")
    @JsonProperty("product_id")
    private long productId;

    @NotNull(message = "expire_date is not null")
    @JsonProperty("expire_date")
    private LocalDateTime expireDate;

    public InventoryTransactionDTO(
        float unitPrice,
        int quantity,
        LocalDateTime transactionDate,
        String note,
        long supplierId,
        long productId,
        LocalDateTime expireDate
    ) {
        this.unitPrice = unitPrice;
        this.quantity = quantity;
        this.transactionDate = transactionDate;
        this.note = note;
        this.supplierId = supplierId;
        this.productId = productId;
        this.expireDate = expireDate;
    }

    public InventoryTransactionDTO() {}

    @Min(value = 0, message = "Price of product must be >= 0")
    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(@Min(value = 0, message = "Price of product must be >= 0") float unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Min(value = 1, message = "Number of products must be >= 1")
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(@Min(value = 1, message = "Number of products must be >= 1") int quantity) {
        this.quantity = quantity;
    }

    public @NotNull(message = "transaction_date is not null") LocalDateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(@NotNull(message = "transaction_date is not null") LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    public @NotBlank(message = "Notes is not blank") String getNote() {
        return note;
    }

    public void setNote(@NotBlank(message = "Notes is not blank") String note) {
        this.note = note;
    }

    @Min(value = 1, message = "Supplier ID must be >= 1 and not null")
    public long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(@Min(value = 1, message = "Supplier ID must be >= 1 and not null") long supplierId) {
        this.supplierId = supplierId;
    }

    @Min(value = 1, message = "Product ID must be >= 1 and not null")
    public long getProductId() {
        return productId;
    }

    public void setProductId(@Min(value = 1, message = "Product ID must be >= 1 and not null") long productId) {
        this.productId = productId;
    }

    public @NotNull(message = "expire_date is not null") LocalDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(@NotNull(message = "expire_date is not null") LocalDateTime expireDate) {
        this.expireDate = expireDate;
    }
}
