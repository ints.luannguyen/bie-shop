package com.ints.bieshop.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class ProductDTO {

    @NotBlank(message = "Product name is not null!")
    @JsonProperty("name")
    private String name;

    @JsonProperty("desc")
    private String desc;

    @Min(value = 0, message = "Price of product must be >= 0")
    @JsonProperty("price")
    private double price;

    @Min(value = 1, message = "Number of category ID must be >= 1 and not null")
    @JsonProperty("category_id")
    private long categoryId;

    @Min(value = 1, message = "Number of supplier ID must be >= 1 and not null")
    @JsonProperty("supplier_id")
    private long supplierId;

    @Min(value = 1, message = "Number of discount ID must be >= 1 and not null")
    @JsonProperty("discount_id")
    private long discountId;

    public ProductDTO(String name, String desc, double price, long categoryId, long supplierId, long discountId) {
        this.name = name;
        this.desc = desc;
        this.price = price;
        this.categoryId = categoryId;
        this.supplierId = supplierId;
        this.discountId = discountId;
    }

    public ProductDTO() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDiscountId() {
        return discountId;
    }

    public void setDiscountId(long discountId) {
        this.discountId = discountId;
    }

    public long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(long supplierId) {
        this.supplierId = supplierId;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
