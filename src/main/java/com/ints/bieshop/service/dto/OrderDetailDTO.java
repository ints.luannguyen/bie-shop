package com.ints.bieshop.service.dto;

import java.io.Serializable;

public class OrderDetailDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Double price;
    private Integer quantity;
    private Boolean isDeleted;
    private Long productId;
    private Long productOptionId;

    public OrderDetailDTO() {}

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getProductOptionId() {
        return productOptionId;
    }

    public void setProductOptionId(Long productOptionId) {
        this.productOptionId = productOptionId;
    }
}
