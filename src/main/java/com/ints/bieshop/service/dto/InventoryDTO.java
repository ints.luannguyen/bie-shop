package com.ints.bieshop.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class InventoryDTO {

    @NotNull(message = "Expire date is not null!")
    @JsonProperty("expire_date")
    private LocalDateTime expireDate;

    @Min(value = 1, message = "Number of product must be >= 1 and not null")
    @JsonProperty("quantity")
    private int quantity;

    @Min(value = 1, message = "Product ID must be >= 1 and not null")
    @JsonProperty("product_id")
    private long productId;

    public InventoryDTO(LocalDateTime expireDate, int quantity, long productId) {
        this.expireDate = expireDate;
        this.quantity = quantity;
        this.productId = productId;
    }

    public @NotNull(message = "Expire date is not null!") LocalDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(@NotNull(message = "Expire date is not null!") LocalDateTime expireDate) {
        this.expireDate = expireDate;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Min(value = 1, message = "Product ID must be >= 1 and not null")
    public long getProductId() {
        return productId;
    }

    public void setProductId(@Min(value = 1, message = "Product ID must be >= 1 and not null") long productId) {
        this.productId = productId;
    }
}
