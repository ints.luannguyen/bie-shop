package com.ints.bieshop.service.dto;

import com.ints.bieshop.domain.enumeration.PaymentStatus;
import java.io.Serializable;
import java.time.Instant;

public class PaymentDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String paymentMethod;
    private Float amount;
    private Instant paymentDate;
    private PaymentStatus status;
    private Boolean isDeleted;

    public PaymentDTO() {}

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Instant getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Instant paymentDate) {
        this.paymentDate = paymentDate;
    }

    public PaymentStatus getStatus() {
        return status;
    }

    public void setStatus(PaymentStatus status) {
        this.status = status;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}
