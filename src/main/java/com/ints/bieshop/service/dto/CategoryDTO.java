package com.ints.bieshop.service.dto;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;

public class CategoryDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Getter
    private long id;

    private String name;
    private String desc;
    private Boolean isDeleted;
    private List<CategoryDTO> childrents;

    public CategoryDTO() {}

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public List<CategoryDTO> getChildrents() {
        return childrents;
    }

    public void setChildrents(List<CategoryDTO> childrents) {
        this.childrents = childrents;
    }

    @Override
    public String toString() {
        return (
            "CategoryDTO{" +
            "id=" +
            id +
            ", name='" +
            name +
            '\'' +
            ", desc='" +
            desc +
            '\'' +
            ", isDeleted=" +
            isDeleted +
            ", childrents=" +
            childrents +
            '}'
        );
    }
}
