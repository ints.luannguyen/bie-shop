package com.ints.bieshop.service;

import com.ints.bieshop.domain.InventoryTransactions;
import com.ints.bieshop.repository.InventoryTransactionsRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.ints.bieshop.domain.InventoryTransactions}.
 */
@Service
@Transactional
public class InventoryTransactionsService {

    private final Logger log = LoggerFactory.getLogger(InventoryTransactionsService.class);

    private final InventoryTransactionsRepository inventoryTransactionsRepository;

    public InventoryTransactionsService(InventoryTransactionsRepository inventoryTransactionsRepository) {
        this.inventoryTransactionsRepository = inventoryTransactionsRepository;
    }

    /**
     * Save a inventoryTransactions.
     *
     * @param inventoryTransactions the entity to save.
     * @return the persisted entity.
     */
    public InventoryTransactions save(InventoryTransactions inventoryTransactions) {
        log.debug("Request to save InventoryTransactions : {}", inventoryTransactions);
        return inventoryTransactionsRepository.save(inventoryTransactions);
    }

    /**
     * Update a inventoryTransactions.
     *
     * @param inventoryTransactions the entity to save.
     * @return the persisted entity.
     */
    public InventoryTransactions update(InventoryTransactions inventoryTransactions) {
        log.debug("Request to update InventoryTransactions : {}", inventoryTransactions);
        return inventoryTransactionsRepository.save(inventoryTransactions);
    }

    /**
     * Partially update a inventoryTransactions.
     *
     * @param inventoryTransactions the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<InventoryTransactions> partialUpdate(InventoryTransactions inventoryTransactions) {
        log.debug("Request to partially update InventoryTransactions : {}", inventoryTransactions);

        return inventoryTransactionsRepository
            .findById(inventoryTransactions.getId())
            .map(existingInventoryTransactions -> {
                if (inventoryTransactions.getQuantity() != null) {
                    existingInventoryTransactions.setQuantity(inventoryTransactions.getQuantity());
                }
                if (inventoryTransactions.getUnitPrice() != null) {
                    existingInventoryTransactions.setUnitPrice(inventoryTransactions.getUnitPrice());
                }
                if (inventoryTransactions.getTransactionDate() != null) {
                    existingInventoryTransactions.setTransactionDate(inventoryTransactions.getTransactionDate());
                }
                if (inventoryTransactions.getNote() != null) {
                    existingInventoryTransactions.setNote(inventoryTransactions.getNote());
                }
                if (inventoryTransactions.getIsDeleted() != null) {
                    existingInventoryTransactions.setIsDeleted(inventoryTransactions.getIsDeleted());
                }

                return existingInventoryTransactions;
            })
            .map(inventoryTransactionsRepository::save);
    }

    /**
     * Get all the inventoryTransactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<InventoryTransactions> findAll(Pageable pageable) {
        log.debug("Request to get all InventoryTransactions");
        return inventoryTransactionsRepository.findAll(pageable);
    }

    /**
     * Get one inventoryTransactions by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<InventoryTransactions> findOne(Long id) {
        log.debug("Request to get InventoryTransactions : {}", id);
        return inventoryTransactionsRepository.findById(id);
    }

    /**
     * Delete the inventoryTransactions by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete InventoryTransactions : {}", id);
        inventoryTransactionsRepository.deleteById(id);
    }
}
