package com.ints.bieshop.service;

import com.ints.bieshop.domain.Inventory;
import com.ints.bieshop.repository.InventoryRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.ints.bieshop.domain.Inventory}.
 */
@Service
@Transactional
public class InventoryService {

    private final Logger log = LoggerFactory.getLogger(InventoryService.class);

    private final InventoryRepository inventoryRepository;

    public InventoryService(InventoryRepository inventoryRepository) {
        this.inventoryRepository = inventoryRepository;
    }

    /**
     * Save a inventory.
     *
     * @param inventory the entity to save.
     * @return the persisted entity.
     */
    public Inventory save(Inventory inventory) {
        log.debug("Request to save Inventory : {}", inventory);
        return inventoryRepository.save(inventory);
    }

    /**
     * Update a inventory.
     *
     * @param inventory the entity to save.
     * @return the persisted entity.
     */
    public Inventory update(Inventory inventory) {
        log.debug("Request to update Inventory : {}", inventory);
        return inventoryRepository.save(inventory);
    }

    /**
     * Partially update a inventory.
     *
     * @param inventory the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Inventory> partialUpdate(Inventory inventory) {
        log.debug("Request to partially update Inventory : {}", inventory);

        return inventoryRepository
            .findById(inventory.getId())
            .map(existingInventory -> {
                if (inventory.getExpireDate() != null) {
                    existingInventory.setExpireDate(inventory.getExpireDate());
                }
                if (inventory.getQuantity() != null) {
                    existingInventory.setQuantity(inventory.getQuantity());
                }
                if (inventory.getIsDeleted() != null) {
                    existingInventory.setIsDeleted(inventory.getIsDeleted());
                }

                return existingInventory;
            })
            .map(inventoryRepository::save);
    }

    /**
     * Get all the inventories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Inventory> findAll(Pageable pageable) {
        log.debug("Request to get all Inventories");
        return inventoryRepository.findAll(pageable);
    }

    /**
     * Get one inventory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Inventory> findOne(Long id) {
        log.debug("Request to get Inventory : {}", id);
        return inventoryRepository.findById(id);
    }

    /**
     * Delete the inventory by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Inventory : {}", id);
        inventoryRepository.deleteById(id);
    }
}
