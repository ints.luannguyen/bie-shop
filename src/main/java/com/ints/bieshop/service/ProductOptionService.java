package com.ints.bieshop.service;

import com.ints.bieshop.domain.ProductOption;
import com.ints.bieshop.repository.ProductOptionRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.ints.bieshop.domain.ProductOption}.
 */
@Service
@Transactional
public class ProductOptionService {

    private final Logger log = LoggerFactory.getLogger(ProductOptionService.class);

    private final ProductOptionRepository productOptionRepository;

    public ProductOptionService(ProductOptionRepository productOptionRepository) {
        this.productOptionRepository = productOptionRepository;
    }

    /**
     * Save a productOption.
     *
     * @param productOption the entity to save.
     * @return the persisted entity.
     */
    public ProductOption save(ProductOption productOption) {
        log.debug("Request to save ProductOption : {}", productOption);
        return productOptionRepository.save(productOption);
    }

    /**
     * Update a productOption.
     *
     * @param productOption the entity to save.
     * @return the persisted entity.
     */
    public ProductOption update(ProductOption productOption) {
        log.debug("Request to update ProductOption : {}", productOption);
        return productOptionRepository.save(productOption);
    }

    /**
     * Partially update a productOption.
     *
     * @param productOption the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ProductOption> partialUpdate(ProductOption productOption) {
        log.debug("Request to partially update ProductOption : {}", productOption);

        return productOptionRepository
            .findById(productOption.getId())
            .map(existingProductOption -> {
                if (productOption.getName() != null) {
                    existingProductOption.setName(productOption.getName());
                }
                if (productOption.getPriceModifier() != null) {
                    existingProductOption.setPriceModifier(productOption.getPriceModifier());
                }
                if (productOption.getUnit() != null) {
                    existingProductOption.setUnit(productOption.getUnit());
                }
                if (productOption.getIsDeleted() != null) {
                    existingProductOption.setIsDeleted(productOption.getIsDeleted());
                }

                return existingProductOption;
            })
            .map(productOptionRepository::save);
    }

    /**
     * Get all the productOptions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ProductOption> findAll(Pageable pageable) {
        log.debug("Request to get all ProductOptions");
        return productOptionRepository.findAll(pageable);
    }

    /**
     * Get one productOption by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ProductOption> findOne(Long id) {
        log.debug("Request to get ProductOption : {}", id);
        return productOptionRepository.findById(id);
    }

    /**
     * Delete the productOption by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ProductOption : {}", id);
        productOptionRepository.deleteById(id);
    }
}
