package com.ints.bieshop.service.extend;

import com.ints.bieshop.domain.User;
import com.ints.bieshop.repository.extend.UserExtendRepository;
import com.ints.bieshop.web.rest.errors.DataNotFoundException;
import com.ints.bieshop.web.rest.extend.InventoryTransactionExtendResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class UserExtendService {

    private final Logger log = LoggerFactory.getLogger(UserExtendService.class);
    private final UserExtendRepository extendRepository;

    public UserExtendService(UserExtendRepository extendRepository) {
        this.extendRepository = extendRepository;
    }

    public User findByLogin(String login) throws DataNotFoundException {
        log.debug("REST find user by login: {}", login);
        return extendRepository.findByLogin(login).orElseThrow(() -> new DataNotFoundException("User not found"));
    }
}
