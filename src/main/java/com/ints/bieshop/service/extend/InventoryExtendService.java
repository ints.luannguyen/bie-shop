package com.ints.bieshop.service.extend;

import com.ints.bieshop.domain.Inventory;
import com.ints.bieshop.domain.Product;
import com.ints.bieshop.repository.extend.InventoryExtendRepository;
import com.ints.bieshop.service.dto.InventoryDTO;
import com.ints.bieshop.service.mapper.DTOToModelMapper;
import com.ints.bieshop.web.rest.errors.DataNotFoundException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class InventoryExtendService {

    private final Logger log = LoggerFactory.getLogger(InventoryExtendService.class);
    private final InventoryExtendRepository inventoryExtendRepository;
    private final ProductExtendService productExtendService;

    public InventoryExtendService(InventoryExtendRepository inventoryExtendRepository, ProductExtendService productExtendService) {
        this.inventoryExtendRepository = inventoryExtendRepository;
        this.productExtendService = productExtendService;
    }

    public Inventory create(InventoryDTO inventoryDTO) throws DataNotFoundException {
        log.debug("Creating new inventory {}", inventoryDTO);
        Product existProduct = this.productExtendService.findById(inventoryDTO.getProductId());
        Inventory existInventory =
            this.inventoryExtendRepository.findByExpireDateAndProduct_Id(
                    inventoryDTO.getExpireDate().toInstant(ZoneOffset.UTC),
                    inventoryDTO.getProductId()
                );
        if (existInventory != null) {
            existInventory.setQuantity(existInventory.getQuantity() + inventoryDTO.getQuantity());
            return this.inventoryExtendRepository.save(existInventory);
        }
        Inventory inventory = new Inventory();
        return this.inventoryExtendRepository.save(DTOToModelMapper.dtoToEntity(inventory, inventoryDTO, existProduct));
    }

    public Inventory findById(long id) throws DataNotFoundException {
        log.debug("Find by inventory by id {}", id);
        return this.inventoryExtendRepository.findById(id).orElseThrow(
                () -> new DataNotFoundException("Inventory not found with id " + id)
            );
    }

    public Inventory update(long id, InventoryDTO inventoryDTO) throws DataNotFoundException {
        log.debug("Update inventory by id {}", id);
        Inventory existInventory = this.findById(id);
        Product existProduct = this.productExtendService.findById(inventoryDTO.getProductId());
        return this.inventoryExtendRepository.save(DTOToModelMapper.dtoToEntity(existInventory, inventoryDTO, existProduct));
    }

    public Inventory delete(long id) throws DataNotFoundException {
        log.debug("Delete inventory by id {}", id);
        Inventory existInventory = this.findById(id);
        existInventory.setIsDeleted(true);
        return this.inventoryExtendRepository.save(existInventory);
    }

    public List<Inventory> getAllUnexpired() {
        log.debug("Get all unexpired inventoríes");
        return this.inventoryExtendRepository.findByExpireDateAfterOrderByExpireDate(LocalDateTime.now().toInstant(ZoneOffset.UTC));
    }
}
