package com.ints.bieshop.service.extend;

import com.ints.bieshop.domain.Product;
import com.ints.bieshop.domain.ProductImage;
import com.ints.bieshop.repository.ProductImageRepository;
import com.ints.bieshop.web.rest.errors.DataNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ProductImageExtendService {

    private final Logger log = LoggerFactory.getLogger(ProductImageExtendService.class);
    private final ProductImageRepository productImageRepository;
    private final ProductExtendService productExtendService;

    public ProductImageExtendService(ProductImageRepository productImageRepository, ProductExtendService productExtendService) {
        this.productImageRepository = productImageRepository;
        this.productExtendService = productExtendService;
    }

    public ProductImage createProductImages(String imageName, long productId) throws DataNotFoundException {
        log.info("Creating product images for {}", imageName);
        Product existProduct = this.productExtendService.findById(productId);
        ProductImage productImage = new ProductImage();
        productImage.setImage(imageName);
        productImage.setIsDeleted(false);
        productImage.setProduct(existProduct);
        return this.productImageRepository.save(productImage);
    }
}
