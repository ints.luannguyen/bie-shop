package com.ints.bieshop.service.extend;

import com.ints.bieshop.domain.Category;
import com.ints.bieshop.repository.extend.CategoryExtendRepository;
import com.ints.bieshop.service.dto.CategoryDTO;
import com.ints.bieshop.web.rest.errors.DataNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CategoryExtendService {

    private final Logger log = LoggerFactory.getLogger(CategoryExtendService.class);

    private final CategoryExtendRepository categoryExtendRepository;

    public CategoryExtendService(CategoryExtendRepository categoryExtendRepository) {
        this.categoryExtendRepository = categoryExtendRepository;
    }

    public List<CategoryDTO> findCategories() {
        log.debug("Request to get all Categories");
        List<Category> listCategoryWithParentIsNull = categoryExtendRepository.findAllCategoryWithParentNull();
        List<Category> listCategoryWithParentNotNull = categoryExtendRepository.findAllWithParentNotNull();
        List<CategoryDTO> listCategories = new ArrayList<>();
        for (Category category : listCategoryWithParentIsNull) {
            CategoryDTO categoryDTO = new CategoryDTO();
            categoryDTO.setId(category.getId());
            categoryDTO.setName(category.getName());
            categoryDTO.setDesc(category.getDesc());
            categoryDTO.setDeleted(category.getIsDeleted());
            List<CategoryDTO> listCategoryDTOChild = new ArrayList<>();
            for (Category categoryChildren : listCategoryWithParentNotNull) {
                if (categoryChildren.getParent().getId().equals(category.getId())) {
                    CategoryDTO categoryDTOChild = new CategoryDTO();
                    categoryDTOChild.setId(categoryChildren.getId());
                    categoryDTOChild.setName(categoryChildren.getName());
                    categoryDTOChild.setDesc(categoryChildren.getDesc());
                    categoryDTOChild.setDeleted(categoryChildren.getIsDeleted());
                    listCategoryDTOChild.add(categoryDTOChild);
                }
            }
            categoryDTO.setChildrents(listCategoryDTOChild);
            listCategories.add(categoryDTO);
        }
        return listCategories;
    }

    public Optional<CategoryDTO> findOne(Long id) {
        log.debug("Request to get Category : {}", id);
        Optional<Category> category = categoryExtendRepository.findById(id);
        CategoryDTO categoryDTO = new CategoryDTO();
        if (category.isPresent()) {
            categoryDTO.setId(category.get().getId());
            categoryDTO.setName(category.get().getName());
            categoryDTO.setDesc(category.get().getDesc());
            categoryDTO.setDeleted(category.get().getIsDeleted());
            if (category.get().getParent() == null) {
                List<Category> listChildren = categoryExtendRepository.findCategoriesByParentId(id);
                List<CategoryDTO> listCategoryDTOChild = new ArrayList<>();
                for (Category categoryChildren : listChildren) {
                    CategoryDTO categoryDTOChild = new CategoryDTO();
                    categoryDTOChild.setId(categoryChildren.getId());
                    categoryDTOChild.setName(categoryChildren.getName());
                    categoryDTOChild.setDesc(categoryChildren.getDesc());
                    categoryDTOChild.setDeleted(categoryChildren.getIsDeleted());
                    listCategoryDTOChild.add(categoryDTOChild);
                }
                categoryDTO.setChildrents(listCategoryDTOChild);
            }
        }
        return Optional.of(categoryDTO);
    }

    @Transactional
    public void delete(Long id) {
        Optional<Category> category = categoryExtendRepository.findById(id);
        if (category.isPresent()) {
            if (category.get().getParent().getId() != null) {
                categoryExtendRepository.deleteById(id);
            } else {
                List<Category> listChildren = categoryExtendRepository.findCategoriesByParentId(id);
                for (Category categoryChildren : listChildren) {
                    categoryExtendRepository.deleteById(categoryChildren.getId());
                }
                categoryExtendRepository.deleteById(id);
            }
        }
    }

    public Category findById(long id) throws DataNotFoundException {
        log.debug("Request to find category by id: {}", id);
        return categoryExtendRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Category not found"));
    }
}
