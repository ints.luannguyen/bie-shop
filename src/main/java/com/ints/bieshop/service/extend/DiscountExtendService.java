package com.ints.bieshop.service.extend;

import com.ints.bieshop.domain.Discount;
import com.ints.bieshop.repository.extend.DiscountExtendRepository;
import com.ints.bieshop.service.dto.DiscountDTO;
import com.ints.bieshop.service.mapper.DTOToModelMapper;
import com.ints.bieshop.web.rest.errors.DataNotFoundException;
import jakarta.persistence.EntityNotFoundException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DiscountExtendService {

    private final Logger log = LoggerFactory.getLogger(DiscountExtendService.class);
    private final DiscountExtendRepository discountRepository;

    public DiscountExtendService(DiscountExtendRepository discountRepository) {
        this.discountRepository = discountRepository;
    }

    public Discount save(DiscountDTO discountDTO) {
        Discount discount = new Discount();
        return discountRepository.save(DTOToModelMapper.dtoToEntity(discountDTO, discount));
    }

    public List<Discount> getAll() {
        log.debug("REST request to get all Discounts");
        return discountRepository.findAll();
    }

    public List<Discount> getByDiscountRate(float discountRate) {
        log.debug("REST request to get discount rate Discounts");
        return discountRepository.findByDiscountRate(discountRate);
    }

    public Discount update(DiscountDTO discountDTO, Long id) {
        log.debug("REST request to update Discounts");
        Discount existDiscount = discountRepository
            .findById(id)
            .orElseThrow(() -> new EntityNotFoundException("Not found Discount with id: " + id));
        return discountRepository.save(DTOToModelMapper.dtoToEntity(discountDTO, existDiscount));
    }

    public Discount delete(Long id) throws DataNotFoundException {
        log.debug("REST request to delete Discounts");
        Discount existDiscount = discountRepository
            .findById(id)
            .orElseThrow(() -> new DataNotFoundException("Not found Discount with id: " + id));
        existDiscount.setIsDeleted(true);
        return discountRepository.save(existDiscount);
    }

    public Discount findById(long id) throws DataNotFoundException {
        log.debug("REST request to find by id discount");
        return discountRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Not found discount with id = " + id));
    }
}
