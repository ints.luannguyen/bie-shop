package com.ints.bieshop.service.extend;

import com.ints.bieshop.domain.Category;
import com.ints.bieshop.domain.Discount;
import com.ints.bieshop.domain.Product;
import com.ints.bieshop.domain.Supplier;
import com.ints.bieshop.repository.ProductRepository;
import com.ints.bieshop.repository.SupplierRepository;
import com.ints.bieshop.service.dto.ProductDTO;
import com.ints.bieshop.service.mapper.DTOToModelMapper;
import com.ints.bieshop.web.rest.errors.DataNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ProductExtendService {

    private final Logger log = LoggerFactory.getLogger(ProductExtendService.class);
    private final ProductRepository productRepository;
    private final CategoryExtendService categoryExtendService;
    private final DiscountExtendService discountExtendService;
    private final SupplierRepository supplierRepository;

    public ProductExtendService(
        ProductRepository productRepository,
        CategoryExtendService categoryExtendService,
        DiscountExtendService discountExtendService,
        SupplierRepository supplierRepository
    ) {
        this.productRepository = productRepository;
        this.categoryExtendService = categoryExtendService;
        this.discountExtendService = discountExtendService;
        this.supplierRepository = supplierRepository;
    }

    public Product create(ProductDTO productDTO) throws DataNotFoundException {
        log.debug("REST request to save Product : {}", productDTO);
        Discount existDiscount = discountExtendService.findById(productDTO.getDiscountId());
        Category existCategory = categoryExtendService.findById(productDTO.getCategoryId());
        Supplier existSupplier = supplierRepository
            .findById(productDTO.getSupplierId())
            .orElseThrow(() -> new DataNotFoundException("Not found supplier with id = " + productDTO.getSupplierId()));
        Product product = new Product();
        return productRepository.save(DTOToModelMapper.dtoToEntity(product, productDTO, existCategory, existSupplier, existDiscount));
    }

    public Page<Product> findAll(PageRequest pageRequest) {
        log.debug("REST request to get all Products");
        return productRepository.findAll(pageRequest);
    }

    public Product findById(long id) throws DataNotFoundException {
        log.debug("REST request to get Product : {}", id);
        return productRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Not found product with id = " + id));
    }

    public Product update(ProductDTO productDTO, long id) throws DataNotFoundException {
        log.debug("REST request to update Product : {}", productDTO);
        Product existProduct = this.findById(id);
        Discount existDiscount = discountExtendService.findById(productDTO.getDiscountId());
        Category existCategory = categoryExtendService.findById(productDTO.getCategoryId());
        Supplier existSupplier = supplierRepository
            .findById(productDTO.getSupplierId())
            .orElseThrow(() -> new DataNotFoundException("Not found supplier with id = " + productDTO.getSupplierId()));
        return this.productRepository.save(
                DTOToModelMapper.dtoToEntity(existProduct, productDTO, existCategory, existSupplier, existDiscount)
            );
    }

    public Product delete(long id) throws DataNotFoundException {
        log.debug("REST request to delete Product : {}", id);
        Product existProduct = this.findById(id);
        existProduct.setIsDeleted(true);
        return productRepository.save(existProduct);
    }

    public void updateProductThumbnail(String thumbnail, Product existingProduct) {
        existingProduct.setThumbnail(thumbnail);
        productRepository.save(existingProduct);
    }
}
