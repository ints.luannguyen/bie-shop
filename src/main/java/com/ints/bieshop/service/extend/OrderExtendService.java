package com.ints.bieshop.service.extend;

import com.ints.bieshop.domain.*;
import com.ints.bieshop.domain.enumeration.OrderStatus;
import com.ints.bieshop.repository.*;
import com.ints.bieshop.repository.extend.OrderExtendRepository;
import com.ints.bieshop.service.UserService;
import com.ints.bieshop.service.dto.OrderDTO;
import com.ints.bieshop.service.dto.OrderDetailDTO;
import com.ints.bieshop.service.dto.PaymentDTO;
import java.time.Instant;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrderExtendService {

    private final Logger log = LoggerFactory.getLogger(OrderExtendService.class);

    private final UserService userService;

    private final OrderExtendRepository orderExtendRepository;

    private final OrderDetailRepository orderDetailRepository;

    private final PaymentRepository paymentRepository;

    private final ProductRepository productRepository;

    private final ProductOptionRepository productOptionRepository;

    public OrderExtendService(
        UserService userService,
        OrderExtendRepository orderExtendRepository,
        OrderDetailRepository orderDetailRepository,
        PaymentRepository paymentRepository,
        ProductRepository productRepository,
        ProductOptionRepository productOptionRepository
    ) {
        this.userService = userService;
        this.orderExtendRepository = orderExtendRepository;
        this.orderDetailRepository = orderDetailRepository;
        this.paymentRepository = paymentRepository;
        this.productRepository = productRepository;
        this.productOptionRepository = productOptionRepository;
    }

    @Transactional(readOnly = true)
    public List<Order> findAllWherePaymentIsNull() {
        log.debug("Request to get all orders where Payment is null");
        return orderExtendRepository.findAll().stream().filter(order -> order.getPayment() == null).toList();
    }

    @Transactional(readOnly = true)
    public Page<Order> findAll(Pageable pageable) {
        log.debug("Request to get all Orders");
        return orderExtendRepository.findAll(pageable);
    }

    public Page<Order> findOrdersByUser(Long userId, Pageable pageable) {
        log.debug("Request to get all Orders By User");
        return orderExtendRepository.findOrdersByUser(userId, pageable);
    }

    public Page<Order> findOrdersByDateRanger(Instant startTime, Instant endTime, Pageable pageable) {
        log.debug("Request to get all Orders By Time");
        return orderExtendRepository.findOrdersByDateRange(startTime, endTime, pageable);
    }

    public Page<Order> findOrdersByStatus(OrderStatus orderStatus, Pageable pageable) {
        log.debug("Request to get all Orders By Status");
        return orderExtendRepository.findOrdersByOrderStatus(orderStatus, pageable);
    }

    @Transactional
    public Order save(OrderDTO orderDTO) {
        log.debug("Request to save OrderDTO : {}", orderDTO);
        Order order = new Order();
        List<OrderDetailDTO> orderDetailDTOS = orderDTO.getOrderDetails();
        PaymentDTO paymentDTO = orderDTO.getPaymentDTO();
        List<OrderDetail> orderDetails = new ArrayList<>();
        for (OrderDetailDTO orderDetailDTO : orderDetailDTOS) {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setPrice(orderDetailDTO.getPrice());
            orderDetail.setQuantity(orderDetailDTO.getQuantity());
            orderDetail.setIsDeleted(orderDetailDTO.getDeleted());

            Optional<Product> product = productRepository.findById(orderDetailDTO.getProductId());
            product.ifPresent(orderDetail::setProduct);

            Optional<ProductOption> productOption = productOptionRepository.findById(orderDetailDTO.getProductOptionId());
            productOption.ifPresent(orderDetail::setProductOption);

            orderDetails.add(orderDetail);
        }
        order.setAddress(orderDTO.getAddress());
        order.setPhoneNumber(orderDTO.getPhoneNumber());
        order.setName(orderDTO.getName());
        order.setOrderStatus(orderDTO.getOrderStatus());
        order.setOrderAt(orderDTO.getOrderAt());
        order.setIsDeleted(orderDTO.getDeleted());
        Set<OrderDetail> hashSet = new HashSet<>(orderDetails);
        order.setOrderDetails(hashSet);

        Optional<User> user = userService.getUserById(orderDTO.getUserId());
        user.ifPresent(order::setUser);

        Payment payment = new Payment();
        payment.setPaymentMethod(paymentDTO.getPaymentMethod());
        payment.setAmount(paymentDTO.getAmount());
        payment.setPaymentDate(paymentDTO.getPaymentDate());
        payment.setStatus(paymentDTO.getStatus());
        payment.setIsDeleted(paymentDTO.getDeleted());
        order.setPayment(payment);

        List<OrderDetail> orderDetailList = new ArrayList<>(order.getOrderDetails());
        if (order.getUser() != null) {
            order = orderExtendRepository.save(order);
            orderDetailRepository.saveAll(orderDetailList);
            paymentRepository.save(payment);
        }
        return order;
    }

    @Transactional
    public Order update(OrderDTO orderDTO) {
        log.debug("Request to update Order : {}", orderDTO);
        return save(orderDTO);
    }

    public void delete(Long id) {
        log.debug("Request to delete Order : {}", id);
        orderExtendRepository.removeOrder(id);
    }
}
