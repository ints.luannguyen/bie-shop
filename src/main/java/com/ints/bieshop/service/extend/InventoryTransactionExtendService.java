package com.ints.bieshop.service.extend;

import com.ints.bieshop.domain.InventoryTransactions;
import com.ints.bieshop.domain.Product;
import com.ints.bieshop.domain.Supplier;
import com.ints.bieshop.domain.User;
import com.ints.bieshop.repository.InventoryTransactionsRepository;
import com.ints.bieshop.repository.SupplierRepository;
import com.ints.bieshop.repository.extend.InventoryTransactionExtendRepository;
import com.ints.bieshop.service.dto.InventoryDTO;
import com.ints.bieshop.service.dto.InventoryTransactionDTO;
import com.ints.bieshop.service.mapper.DTOToModelMapper;
import com.ints.bieshop.web.rest.errors.DataNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InventoryTransactionExtendService {

    private final Logger log = LoggerFactory.getLogger(InventoryTransactionExtendService.class);
    private final InventoryTransactionExtendRepository inventoryTransactionsRepository;
    private final InventoryExtendService inventoryExtendService;
    private final ProductExtendService productExtendService;
    private final SupplierRepository supplierRepository;

    public InventoryTransactionExtendService(
        InventoryTransactionExtendRepository inventoryTransactionsRepository,
        InventoryExtendService inventoryExtendService,
        ProductExtendService productExtendService,
        SupplierRepository supplierRepository
    ) {
        this.inventoryTransactionsRepository = inventoryTransactionsRepository;
        this.inventoryExtendService = inventoryExtendService;
        this.productExtendService = productExtendService;
        this.supplierRepository = supplierRepository;
    }

    @Transactional
    public InventoryTransactions store(InventoryTransactionDTO inventoryTransactionDTO, User currentUser) throws DataNotFoundException {
        log.debug("Store InventoryTransactionDTO: {}", inventoryTransactionDTO);
        Product existProduct = this.productExtendService.findById(inventoryTransactionDTO.getProductId());
        Supplier existSupplier =
            this.supplierRepository.findById(inventoryTransactionDTO.getSupplierId()).orElseThrow(
                    () -> new DataNotFoundException("Not found supplier with id = " + inventoryTransactionDTO.getSupplierId())
                );
        InventoryDTO inventoryDTO = new InventoryDTO(
            inventoryTransactionDTO.getExpireDate(),
            inventoryTransactionDTO.getQuantity(),
            inventoryTransactionDTO.getProductId()
        );
        this.inventoryExtendService.create(inventoryDTO);
        InventoryTransactions inventoryTransactions = new InventoryTransactions();
        return this.inventoryTransactionsRepository.save(
                DTOToModelMapper.dtoToEntity(existProduct, existSupplier, inventoryTransactionDTO, inventoryTransactions, currentUser)
            );
    }

    public List<InventoryTransactions> findByTransactionDate(LocalDateTime transactionDateTime) {
        log.debug("REST find inventory_transaction by transaction date: {}", transactionDateTime);
        return this.inventoryTransactionsRepository.findByTransactionDate(transactionDateTime.toInstant(ZoneOffset.UTC));
    }

    public List<InventoryTransactions> findBySupplierId(String phoneNumber) {
        log.debug("REST find inventory_transaction by product id: {}", phoneNumber);
        return this.inventoryTransactionsRepository.findBySupplier_Phone(phoneNumber);
    }
}
