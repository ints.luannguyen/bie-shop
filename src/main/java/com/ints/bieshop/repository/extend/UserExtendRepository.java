package com.ints.bieshop.repository.extend;

import com.ints.bieshop.domain.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserExtendRepository extends JpaRepository<User, Long> {
    Optional<User> findByLogin(String username);
}
