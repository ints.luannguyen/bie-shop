package com.ints.bieshop.repository.extend;

import com.ints.bieshop.domain.Order;
import com.ints.bieshop.domain.enumeration.OrderStatus;
import jakarta.transaction.Transactional;
import java.time.Instant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderExtendRepository extends JpaRepository<Order, Long> {
    @Query("SELECT c FROM Order c WHERE c.user.id = :userId")
    Page<Order> findOrdersByUser(@Param("userId") Long userId, Pageable pageable);

    @Query("SELECT c FROM Order c WHERE c.orderAt >= :startTime AND c.orderAt <= :endTime")
    Page<Order> findOrdersByDateRange(@Param("startTime") Instant startTime, @Param("endTime") Instant endTime, Pageable pageable);

    @Modifying
    @Transactional
    @Query("UPDATE Order o SET o.isDeleted = true WHERE o.id = :orderId")
    void removeOrder(@Param("orderId") Long orderId);

    @Query("SELECT c FROM Order c WHERE c.orderStatus = :status")
    Page<Order> findOrdersByOrderStatus(@Param("status") OrderStatus status, Pageable pageable);
}
