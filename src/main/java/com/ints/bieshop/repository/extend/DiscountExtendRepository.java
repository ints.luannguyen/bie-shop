package com.ints.bieshop.repository.extend;

import com.ints.bieshop.domain.Discount;
import java.util.List;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiscountExtendRepository extends JpaRepository<Discount, Long> {
    @EntityGraph(attributePaths = "products")
    List<Discount> findByDiscountRate(float discountRate);
}
