package com.ints.bieshop.repository.extend;

import com.ints.bieshop.domain.Category;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryExtendRepository extends JpaRepository<Category, Long> {
    @Query("SELECT c FROM Category c WHERE c.parent IS NULL")
    List<Category> findAllCategoryWithParentNull();

    @Query("SELECT c FROM Category c WHERE c.parent IS NOT NULL")
    List<Category> findAllWithParentNotNull();

    List<Category> findCategoriesByParentId(Long id);
}
