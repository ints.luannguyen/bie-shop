package com.ints.bieshop.repository.extend;

import com.ints.bieshop.domain.InventoryTransactions;
import java.time.Instant;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryTransactionExtendRepository extends JpaRepository<InventoryTransactions, Long> {
    List<InventoryTransactions> findByTransactionDate(Instant transactionDate);
    List<InventoryTransactions> findBySupplier_Phone(String phone);
}
