package com.ints.bieshop.repository.extend;

import com.ints.bieshop.domain.Inventory;
import java.time.Instant;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryExtendRepository extends JpaRepository<Inventory, Long> {
    List<Inventory> findByExpireDateAfterOrderByExpireDate(Instant today);
    Inventory findByExpireDateAndProduct_Id(Instant today, Long productId);
}
