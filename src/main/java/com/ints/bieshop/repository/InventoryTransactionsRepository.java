package com.ints.bieshop.repository;

import com.ints.bieshop.domain.InventoryTransactions;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the InventoryTransactions entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InventoryTransactionsRepository extends JpaRepository<InventoryTransactions, Long> {
    @Query(
        "select inventoryTransactions from InventoryTransactions inventoryTransactions where inventoryTransactions.user.login = ?#{authentication.name}"
    )
    List<InventoryTransactions> findByUserIsCurrentUser();
}
