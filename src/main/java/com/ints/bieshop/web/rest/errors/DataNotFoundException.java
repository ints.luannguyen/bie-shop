package com.ints.bieshop.web.rest.errors;

public class DataNotFoundException extends Exception {

    public DataNotFoundException(String message) {
        super(message);
    }
}
