package com.ints.bieshop.web.rest.extend;

import com.ints.bieshop.domain.Product;
import com.ints.bieshop.domain.ProductImage;
import com.ints.bieshop.service.extend.ProductExtendService;
import com.ints.bieshop.service.extend.ProductImageExtendService;
import com.ints.bieshop.web.response.ApiResponse;
import com.ints.bieshop.web.rest.errors.DataNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("${api.prefix}/files")
public class FileUploadResource {

    private final Logger log = LoggerFactory.getLogger(FileUploadResource.class);
    private final ProductExtendService productExtendService;
    private final ProductImageExtendService productImageExtendService;

    public FileUploadResource(ProductExtendService productExtendService, ProductImageExtendService productImageExtendService) {
        this.productExtendService = productExtendService;
        this.productImageExtendService = productImageExtendService;
    }

    @PostMapping(value = "product", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> uploadThumbnail(@ModelAttribute("files") List<MultipartFile> files, @Param("id") Long id) {
        try {
            log.info("Uploading thumbnail for product {}", id);
            Product product = this.productExtendService.findById(id);
            List<String> images = this.uploadImages(files, product);
            return ResponseEntity.ok(new ApiResponse<>(HttpStatus.OK.toString(), "Successfully", images));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ApiResponse<>("Failure", "Something wrong!", e.getMessage()));
        }
    }

    private List<String> uploadImages(List<MultipartFile> files, Product product) throws IOException, DataNotFoundException {
        files = files == null ? new ArrayList<>() : files;
        List<String> productImages = new ArrayList<>();
        if (files.size() > ProductImage.MAXIMUM_IMAGES_PER_PRODUCT) {
            log.error("You can only upload a maximum of " + ProductImage.MAXIMUM_IMAGES_PER_PRODUCT + " images!");
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                "You can only upload a maximum of " + ProductImage.MAXIMUM_IMAGES_PER_PRODUCT + " images!"
            );
        }
        log.info("Number of files: {} ", files.size());
        for (MultipartFile file : files) {
            if (file != null) {
                if (file.getSize() == 0) {
                    continue;
                }
                validatorFile(file);
                String filename = this.storeFiles(file);
                if (product.getThumbnail() == null || product.getThumbnail().isEmpty()) {
                    product.setThumbnail(filename);
                    productExtendService.updateProductThumbnail(filename, product);
                }
                ProductImage productImage = productImageExtendService.createProductImages(filename, product.getId());
                productImages.add(productImage.getImage());
            }
        }
        return productImages;
    }

    private String storeFiles(MultipartFile file) throws IOException {
        log.debug("Store files started");
        String filename = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String uniqueFilename = UUID.randomUUID() + "_" + filename;
        java.nio.file.Path uploadDir = Paths.get("uploads");
        if (!Files.exists(uploadDir)) {
            Files.createDirectories(uploadDir);
        }
        java.nio.file.Path destination = Paths.get(uploadDir.toString(), uniqueFilename);
        Files.copy(file.getInputStream(), destination, StandardCopyOption.REPLACE_EXISTING);
        return uniqueFilename;
    }

    private void validatorFile(MultipartFile file) {
        if (file.getSize() > ProductImage.MAXIMUM_IMAGES_SIZE) {
            log.error("File is to large!");
            throw new ResponseStatusException(HttpStatus.PAYLOAD_TOO_LARGE, "File is too large!");
        }
        String contentType = file.getContentType();
        if (contentType == null || !contentType.startsWith("image/")) {
            log.error("File must be an image!");
            throw new ResponseStatusException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "File must be an image!");
        }
    }
}
