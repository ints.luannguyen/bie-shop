package com.ints.bieshop.web.rest;

import com.ints.bieshop.domain.InventoryTransactions;
import com.ints.bieshop.repository.InventoryTransactionsRepository;
import com.ints.bieshop.service.InventoryTransactionsService;
import com.ints.bieshop.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.ints.bieshop.domain.InventoryTransactions}.
 */
@RestController
@RequestMapping("/api/inventory-transactions")
public class InventoryTransactionsResource {

    private final Logger log = LoggerFactory.getLogger(InventoryTransactionsResource.class);

    private static final String ENTITY_NAME = "inventoryTransactions";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InventoryTransactionsService inventoryTransactionsService;

    private final InventoryTransactionsRepository inventoryTransactionsRepository;

    public InventoryTransactionsResource(
        InventoryTransactionsService inventoryTransactionsService,
        InventoryTransactionsRepository inventoryTransactionsRepository
    ) {
        this.inventoryTransactionsService = inventoryTransactionsService;
        this.inventoryTransactionsRepository = inventoryTransactionsRepository;
    }

    /**
     * {@code POST  /inventory-transactions} : Create a new inventoryTransactions.
     *
     * @param inventoryTransactions the inventoryTransactions to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new inventoryTransactions, or with status {@code 400 (Bad Request)} if the inventoryTransactions has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<InventoryTransactions> createInventoryTransactions(@RequestBody InventoryTransactions inventoryTransactions)
        throws URISyntaxException {
        log.debug("REST request to save InventoryTransactions : {}", inventoryTransactions);
        if (inventoryTransactions.getId() != null) {
            throw new BadRequestAlertException("A new inventoryTransactions cannot already have an ID", ENTITY_NAME, "idexists");
        }
        inventoryTransactions = inventoryTransactionsService.save(inventoryTransactions);
        return ResponseEntity.created(new URI("/api/inventory-transactions/" + inventoryTransactions.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, inventoryTransactions.getId().toString()))
            .body(inventoryTransactions);
    }

    /**
     * {@code PUT  /inventory-transactions/:id} : Updates an existing inventoryTransactions.
     *
     * @param id the id of the inventoryTransactions to save.
     * @param inventoryTransactions the inventoryTransactions to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated inventoryTransactions,
     * or with status {@code 400 (Bad Request)} if the inventoryTransactions is not valid,
     * or with status {@code 500 (Internal Server Error)} if the inventoryTransactions couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<InventoryTransactions> updateInventoryTransactions(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody InventoryTransactions inventoryTransactions
    ) throws URISyntaxException {
        log.debug("REST request to update InventoryTransactions : {}, {}", id, inventoryTransactions);
        if (inventoryTransactions.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, inventoryTransactions.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!inventoryTransactionsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        inventoryTransactions = inventoryTransactionsService.update(inventoryTransactions);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, inventoryTransactions.getId().toString()))
            .body(inventoryTransactions);
    }

    /**
     * {@code PATCH  /inventory-transactions/:id} : Partial updates given fields of an existing inventoryTransactions, field will ignore if it is null
     *
     * @param id the id of the inventoryTransactions to save.
     * @param inventoryTransactions the inventoryTransactions to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated inventoryTransactions,
     * or with status {@code 400 (Bad Request)} if the inventoryTransactions is not valid,
     * or with status {@code 404 (Not Found)} if the inventoryTransactions is not found,
     * or with status {@code 500 (Internal Server Error)} if the inventoryTransactions couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<InventoryTransactions> partialUpdateInventoryTransactions(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody InventoryTransactions inventoryTransactions
    ) throws URISyntaxException {
        log.debug("REST request to partial update InventoryTransactions partially : {}, {}", id, inventoryTransactions);
        if (inventoryTransactions.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, inventoryTransactions.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!inventoryTransactionsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<InventoryTransactions> result = inventoryTransactionsService.partialUpdate(inventoryTransactions);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, inventoryTransactions.getId().toString())
        );
    }

    /**
     * {@code GET  /inventory-transactions} : get all the inventoryTransactions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of inventoryTransactions in body.
     */
    @GetMapping("")
    public ResponseEntity<List<InventoryTransactions>> getAllInventoryTransactions(
        @org.springdoc.core.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of InventoryTransactions");
        Page<InventoryTransactions> page = inventoryTransactionsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /inventory-transactions/:id} : get the "id" inventoryTransactions.
     *
     * @param id the id of the inventoryTransactions to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the inventoryTransactions, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<InventoryTransactions> getInventoryTransactions(@PathVariable("id") Long id) {
        log.debug("REST request to get InventoryTransactions : {}", id);
        Optional<InventoryTransactions> inventoryTransactions = inventoryTransactionsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(inventoryTransactions);
    }

    /**
     * {@code DELETE  /inventory-transactions/:id} : delete the "id" inventoryTransactions.
     *
     * @param id the id of the inventoryTransactions to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteInventoryTransactions(@PathVariable("id") Long id) {
        log.debug("REST request to delete InventoryTransactions : {}", id);
        inventoryTransactionsService.delete(id);
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
