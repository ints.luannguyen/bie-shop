package com.ints.bieshop.web.rest.extend;

import com.ints.bieshop.domain.Discount;
import com.ints.bieshop.security.AuthoritiesConstants;
import com.ints.bieshop.service.dto.DiscountDTO;
import com.ints.bieshop.service.extend.DiscountExtendService;
import com.ints.bieshop.web.response.ApiResponse;
import jakarta.validation.Valid;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;

@RestController
@RequestMapping("${api.prefix}/discount")
public class DiscountExtendResource {

    private final Logger log = LoggerFactory.getLogger(DiscountExtendResource.class);
    private final DiscountExtendService discountService;
    private static final String ENTITY_NAME = "discount";

    public DiscountExtendResource(DiscountExtendService discountService) {
        this.discountService = discountService;
    }

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    @PostMapping
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> createDiscount(@Valid @RequestBody DiscountDTO discountDTO, BindingResult result) {
        try {
            if (result.hasErrors()) {
                List<String> errorMess = result.getFieldErrors().stream().map(FieldError::getDefaultMessage).toList();
                return ResponseEntity.badRequest()
                    .body(new ApiResponse<List<String>>(HttpStatus.BAD_REQUEST.toString(), "Validation failed!", errorMess));
            }
            log.debug("REST request to save Discount : {}", discountDTO);
            Discount newDiscount = discountService.save(discountDTO);
            return ResponseEntity.created(new URI("/api/v1/discount/" + newDiscount.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, newDiscount.getId().toString()))
                .body(new ApiResponse<>(HttpStatus.CREATED.toString(), "Successfully", newDiscount));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ApiResponse<String>(HttpStatus.BAD_REQUEST.toString(), e.getMessage(), null));
        }
    }

    @GetMapping("/all")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> getAllDiscounts() {
        log.debug("REST request to get all Discounts");
        List<Discount> discounts = discountService.getAll();
        return ResponseEntity.ok().body(new ApiResponse<>(HttpStatus.CREATED.toString(), "Get all discount!", discounts));
    }

    @GetMapping("/rate")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> getByDiscountRate(@Param("discountRate") Float discountRate) {
        log.debug("REST request to get by discount rate Discounts");
        List<Discount> discounts = discountService.getByDiscountRate(discountRate);
        return ResponseEntity.ok().body(new ApiResponse<>(HttpStatus.OK.toString(), "Get discount rate discount!", discounts));
    }

    @PutMapping
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> updateDiscount(@Valid @RequestBody DiscountDTO discountDTO, @Param("id") long id, BindingResult result) {
        try {
            if (result.hasErrors()) {
                List<String> errorMess = result.getFieldErrors().stream().map(FieldError::getDefaultMessage).toList();
                return ResponseEntity.badRequest().body(new ApiResponse<>("Failure", "Validation failed", errorMess));
            }
            log.debug("REST request to update Discount : {}", discountDTO);
            Discount newDiscount = discountService.update(discountDTO, id);
            return ResponseEntity.ok(new ApiResponse<>(HttpStatus.OK.toString(), "Update discount!", newDiscount));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ApiResponse<>("Failure", "Validation failed", e.getMessage()));
        }
    }

    @DeleteMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> delete(@Param("id") long id) {
        try {
            log.debug("REST request to delete Discounts");
            Discount discount = discountService.delete(id);
            Map<String, Long> result = new HashMap<>();
            result.put("discount_id", discount.getId());
            return ResponseEntity.ok().body(new ApiResponse<>(HttpStatus.OK.toString(), "Delete discount!", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                .body(new ApiResponse<>(HttpStatus.BAD_REQUEST.toString(), "Delete discount!", e.getMessage()));
        }
    }
}
