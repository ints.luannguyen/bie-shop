package com.ints.bieshop.web.rest.extend;

import com.ints.bieshop.domain.Inventory;
import com.ints.bieshop.domain.InventoryTransactions;
import com.ints.bieshop.domain.User;
import com.ints.bieshop.security.SecurityUtils;
import com.ints.bieshop.service.dto.InventoryTransactionDTO;
import com.ints.bieshop.service.extend.InventoryTransactionExtendService;
import com.ints.bieshop.service.extend.UserExtendService;
import com.ints.bieshop.web.response.ApiResponse;
import com.ints.bieshop.web.rest.errors.DataNotFoundException;
import jakarta.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${api.prefix}/inventory-transaction")
public class InventoryTransactionExtendResource {

    private final Logger log = LoggerFactory.getLogger(InventoryTransactionExtendResource.class);

    private final InventoryTransactionExtendService inventoryTransactionExtendService;
    private final UserExtendService userExtendService;

    public InventoryTransactionExtendResource(
        InventoryTransactionExtendService inventoryTransactionExtendService,
        UserExtendService userExtendService
    ) {
        this.inventoryTransactionExtendService = inventoryTransactionExtendService;
        this.userExtendService = userExtendService;
    }

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody InventoryTransactionDTO inventoryTransactionDTO, BindingResult result) {
        try {
            if (result.hasErrors()) {
                List<String> errorMess = result.getFieldErrors().stream().map(FieldError::getDefaultMessage).toList();
                return ResponseEntity.badRequest()
                    .body(new ApiResponse<>(HttpStatus.BAD_REQUEST.toString(), "Validation failed!", errorMess));
            }
            User currentUser = this.getCurrent();
            log.debug("Creating new inventory transaction: {}", inventoryTransactionDTO);
            InventoryTransactions inventoryTransactions =
                this.inventoryTransactionExtendService.store(inventoryTransactionDTO, currentUser);
            return ResponseEntity.ok().body(new ApiResponse<>(HttpStatus.CREATED.toString(), "Successfully", inventoryTransactions));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ApiResponse<String>(HttpStatus.BAD_REQUEST.toString(), e.getMessage(), null));
        }
    }

    @GetMapping
    public ResponseEntity<?> getByTransactionDate(@Param("transactionDate") String transactionDate) {
        log.debug("REST get inventory_transaction by transactionDate");
        LocalDateTime dateTime = LocalDateTime.parse(transactionDate, DateTimeFormatter.ISO_DATE_TIME);
        return ResponseEntity.ok()
            .body(
                new ApiResponse<>(
                    HttpStatus.OK.toString(),
                    "Successfully",
                    inventoryTransactionExtendService.findByTransactionDate(dateTime)
                )
            );
    }

    private User getCurrent() throws DataNotFoundException {
        log.debug("Get current user by JWT!");
        String currentUserLogin = SecurityUtils.getCurrentUserLogin()
            .orElseThrow(() -> new DataNotFoundException("Current user is not logged in"));
        return this.userExtendService.findByLogin(currentUserLogin);
    }
}
