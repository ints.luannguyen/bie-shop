package com.ints.bieshop.web.rest.extend;

import com.ints.bieshop.domain.Inventory;
import com.ints.bieshop.service.dto.InventoryDTO;
import com.ints.bieshop.service.extend.InventoryExtendService;
import com.ints.bieshop.web.response.ApiResponse;
import jakarta.validation.Valid;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${api.prefix}/inventory")
public class InventoryExtendResource {

    private final Logger log = LoggerFactory.getLogger(InventoryExtendResource.class);
    private final InventoryExtendService inventoryExtendService;

    public InventoryExtendResource(InventoryExtendService inventoryExtendService) {
        this.inventoryExtendService = inventoryExtendService;
    }

    @PostMapping
    public ResponseEntity<?> createInventory(@Valid @RequestBody InventoryDTO inventoryDTO, BindingResult result) {
        try {
            if (result.hasErrors()) {
                List<String> errorMess = result.getFieldErrors().stream().map(FieldError::getDefaultMessage).toList();
                return ResponseEntity.badRequest()
                    .body(new ApiResponse<>(HttpStatus.BAD_REQUEST.toString(), "Validation failed!", errorMess));
            }
            log.debug("REST request to save Inventory: {}", inventoryDTO);
            Inventory inventory = this.inventoryExtendService.create(inventoryDTO);
            return ResponseEntity.ok().body(new ApiResponse<>(HttpStatus.CREATED.toString(), "Successfully", inventory));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ApiResponse<String>(HttpStatus.BAD_REQUEST.toString(), e.getMessage(), null));
        }
    }

    @PutMapping
    public ResponseEntity<?> updateInventory(@Param("id") long id, @Valid @RequestBody InventoryDTO inventoryDTO, BindingResult result) {
        try {
            if (result.hasErrors()) {
                List<String> errorMess = result.getFieldErrors().stream().map(FieldError::getDefaultMessage).toList();
                return ResponseEntity.badRequest()
                    .body(new ApiResponse<>(HttpStatus.BAD_REQUEST.toString(), "Validation failed!", errorMess));
            }
            log.debug("REST request to update Inventory: {}", inventoryDTO);
            Inventory inventory = this.inventoryExtendService.update(id, inventoryDTO);
            return ResponseEntity.ok().body(new ApiResponse<>(HttpStatus.CREATED.toString(), "Successfully", inventory));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ApiResponse<String>(HttpStatus.BAD_REQUEST.toString(), e.getMessage(), null));
        }
    }

    @GetMapping
    public ResponseEntity<?> getAllUnexpired() {
        log.debug("REST request to get all Unexpired Inventories");
        return ResponseEntity.ok()
            .body(
                new ApiResponse<>(HttpStatus.OK.toString(), "Get all unexpired inventories!", this.inventoryExtendService.getAllUnexpired())
            );
    }

    @DeleteMapping
    public ResponseEntity<?> deleteInventory(@Param("id") long id) {
        try {
            log.debug("REST request to delete Inventory: {}", id);
            return ResponseEntity.ok()
                .body(
                    new ApiResponse<>(HttpStatus.OK.toString(), "Get all unexpired inventories!", this.inventoryExtendService.delete(id))
                );
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ApiResponse<String>(HttpStatus.BAD_REQUEST.toString(), e.getMessage(), null));
        }
    }
}
