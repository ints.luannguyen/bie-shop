package com.ints.bieshop.web.rest.extend;

import com.ints.bieshop.domain.*;
import com.ints.bieshop.domain.enumeration.OrderStatus;
import com.ints.bieshop.repository.extend.OrderExtendRepository;
import com.ints.bieshop.service.dto.OrderDTO;
import com.ints.bieshop.service.extend.OrderExtendService;
import com.ints.bieshop.web.rest.errors.BadRequestAlertException;
import jakarta.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;

@RestController
@RequestMapping("${api.prefix}/orders")
public class OrderExtendResource {

    private final Logger log = LoggerFactory.getLogger(OrderExtendResource.class);

    private static final String ENTITY_NAME = "category";
    private final OrderExtendRepository orderExtendRepository;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderExtendService orderExtendService;

    public OrderExtendResource(OrderExtendService orderExtendService, OrderExtendRepository orderExtendRepository) {
        this.orderExtendService = orderExtendService;
        this.orderExtendRepository = orderExtendRepository;
    }

    @PostMapping("")
    public ResponseEntity<Order> createOrder(@Valid @RequestBody OrderDTO orderDTO) throws URISyntaxException {
        log.debug("REST request to save OrderDTO : {}", orderDTO);
        if (orderDTO.getId() != null) {
            throw new BadRequestAlertException("A new order cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Order order = orderExtendService.save(orderDTO);
        return ResponseEntity.created(new URI("/api/orders/" + order.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, order.getId().toString()))
            .body(order);
    }

    @GetMapping("")
    public ResponseEntity<List<Order>> getAllOrders(@org.springdoc.core.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of Orders");
        Page<Order> page = orderExtendService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/timerange")
    public ResponseEntity<List<Order>> getAllOrdersByTimeRanger(
        @org.springdoc.core.annotations.ParameterObject Pageable pageable,
        @RequestParam(value = "startTime", required = false) String startTime,
        @RequestParam(value = "endTime", required = false) String endTime
    ) {
        log.debug("REST request to get a page of Orders by time range");
        Instant startInstant = Instant.now();
        Instant endInstant = Instant.now();

        if (startTime != null) {
            startInstant = Instant.parse(startTime);
        }
        if (endTime != null) {
            endInstant = Instant.parse(endTime);
        }

        Page<Order> page = orderExtendService.findOrdersByDateRanger(startInstant, endInstant, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/orderstatus/{status}")
    public ResponseEntity<List<Order>> getOrderByStatus(
        @org.springdoc.core.annotations.ParameterObject Pageable pageable,
        @PathVariable("status") OrderStatus orderStatus
    ) {
        log.debug("REST request to get a page of Orders By Status : {}", orderStatus);
        Page<Order> page = orderExtendService.findOrdersByStatus(orderStatus, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<List<Order>> getAllOrdersByUser(
        @org.springdoc.core.annotations.ParameterObject Pageable pageable,
        @PathVariable("userId") Long userId
    ) {
        log.debug("REST request to get a page of Orders by User");
        Page<Order> page = orderExtendService.findOrdersByUser(userId, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Order> updateOrder(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody OrderDTO orderDTO
    ) {
        log.debug("REST request to update Order : {}, {}", id, orderDTO);
        if (orderDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, orderDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!orderExtendRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Order order = orderExtendService.update(orderDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, order.getId().toString()))
            .body(order);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteOrder(@PathVariable("id") Long id) {
        log.debug("REST request to delete Order : {}", id);
        orderExtendService.delete(id);
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
