package com.ints.bieshop.web.rest.extend;

import com.ints.bieshop.domain.Product;
import com.ints.bieshop.service.dto.ProductDTO;
import com.ints.bieshop.service.extend.ProductExtendService;
import com.ints.bieshop.web.response.ApiResponse;
import com.ints.bieshop.web.response.PaginationResponse;
import jakarta.validation.Valid;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${api.prefix}/product")
//@RequiredArgsConstructor
public class ProductExtendResource {

    private final Logger log = LoggerFactory.getLogger(ProductExtendResource.class);

    private final ProductExtendService productService;

    public ProductExtendResource(ProductExtendService productService) {
        this.productService = productService;
    }

    @PostMapping
    public ResponseEntity<?> createProduct(@Valid @RequestBody ProductDTO productDTO, BindingResult result) {
        try {
            if (result.hasErrors()) {
                List<String> errorMess = result.getFieldErrors().stream().map(FieldError::getDefaultMessage).toList();
                return ResponseEntity.badRequest()
                    .body(new ApiResponse<>(HttpStatus.BAD_REQUEST.toString(), "Validation failed", errorMess));
            }
            Product newProduct = this.productService.create(productDTO);

            log.debug("REST request to save Product : {}", productDTO);
            return ResponseEntity.ok().body(new ApiResponse<>(HttpStatus.CREATED.toString(), "Successfully", newProduct));
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                .body(new ApiResponse<>(HttpStatus.BAD_REQUEST.toString(), "Validation failed", e.getMessage()));
        }
    }

    @GetMapping("/images/{thumbnail}")
    public ResponseEntity<?> getImageOfProduct(@PathVariable("thumbnail") String thumbnail) {
        try {
            Path imgPath = Paths.get("uploads/" + thumbnail);
            UrlResource urlResource = new UrlResource(imgPath.toUri());
            if (urlResource.exists()) {
                return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(urlResource);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ApiResponse<>("Failure", "Something wrong!", e.getMessage()));
        }
    }

    @PutMapping
    public ResponseEntity<?> updateProduct(@Valid @RequestBody ProductDTO productDTO, @Param("id") long id, BindingResult result) {
        try {
            if (result.hasErrors()) {
                List<String> errorMess = result.getFieldErrors().stream().map(FieldError::getDefaultMessage).toList();
                return ResponseEntity.badRequest()
                    .body(new ApiResponse<>(HttpStatus.BAD_REQUEST.toString(), "Validation failed", errorMess));
            }
            log.debug("REST request to update Product : {}", productDTO);
            Product newProduct = this.productService.update(productDTO, id);
            return ResponseEntity.ok().body(new ApiResponse<>(HttpStatus.OK.toString(), "Successfully", newProduct));
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                .body(new ApiResponse<>(HttpStatus.BAD_REQUEST.toString(), "Validation failed", e.getMessage()));
        }
    }

    @GetMapping
    public ResponseEntity<?> getProducts(@RequestParam("page") int page, @RequestParam("limit") int limit) {
        log.debug("REST request to get all products");
        PageRequest pageRequest = PageRequest.of(page, limit, Sort.by("id").descending());
        Page<Product> products = this.productService.findAll(pageRequest);

        return ResponseEntity.ok(
            new ApiResponse<>(
                HttpStatus.OK.toString(),
                "Success",
                new PaginationResponse<>(products.getTotalPages(), products.getTotalElements(), page, limit, products.stream().toList())
            )
        );
    }

    @DeleteMapping
    public ResponseEntity<?> deleteProduct(@Param("id") long id) {
        try {
            log.debug("REST request to delete product : {}", id);
            Product product = this.productService.delete(id);
            return ResponseEntity.ok().body(new ApiResponse<>(HttpStatus.OK.toString(), "Delete successfully!", product));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ApiResponse<>("Failure", "Something wrong!", e.getMessage()));
        }
    }
}
