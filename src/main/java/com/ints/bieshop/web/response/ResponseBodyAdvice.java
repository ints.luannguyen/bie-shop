package com.ints.bieshop.web.response;

import jakarta.servlet.http.HttpServletResponseWrapper;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;

//@ControllerAdvice
public class ResponseBodyAdvice implements org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice<Object> {

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(
        Object body,
        MethodParameter returnType,
        MediaType selectedContentType,
        Class<? extends HttpMessageConverter<?>> selectedConverterType,
        ServerHttpRequest request,
        ServerHttpResponse response
    ) {
        if (body instanceof HttpServletResponseWrapper) {
            // Do not modify the response if it's already wrapped
            return body;
        }

        // Wrap the response in a ResponseWrapper
        String status = HttpStatus.OK.toString();
        String message = "Success";
        String error = null;

        if (body instanceof ResponseEntity<?> responseEntity) {
            status = responseEntity.getStatusCode().toString();
            message = responseEntity.getStatusCode().toString();
        }

        if (body instanceof Exception exception) {
            status = HttpStatus.INTERNAL_SERVER_ERROR.toString();
            message = "Error occurred";
            error = exception.getMessage();
        }

        return new ApiResponse<>(status, message, error, body);
    }
}
