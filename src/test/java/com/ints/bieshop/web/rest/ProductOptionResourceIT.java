package com.ints.bieshop.web.rest;

import static com.ints.bieshop.domain.ProductOptionAsserts.*;
import static com.ints.bieshop.web.rest.TestUtil.createUpdateProxyForBean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ints.bieshop.IntegrationTest;
import com.ints.bieshop.domain.ProductOption;
import com.ints.bieshop.repository.ProductOptionRepository;
import jakarta.persistence.EntityManager;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ProductOptionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ProductOptionResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Double DEFAULT_PRICE_MODIFIER = 1D;
    private static final Double UPDATED_PRICE_MODIFIER = 2D;

    private static final String DEFAULT_UNIT = "AAAAAAAAAA";
    private static final String UPDATED_UNIT = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_DELETED = false;
    private static final Boolean UPDATED_IS_DELETED = true;

    private static final String ENTITY_API_URL = "/api/product-options";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private ProductOptionRepository productOptionRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProductOptionMockMvc;

    private ProductOption productOption;

    private ProductOption insertedProductOption;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductOption createEntity(EntityManager em) {
        ProductOption productOption = new ProductOption()
            .name(DEFAULT_NAME)
            .priceModifier(DEFAULT_PRICE_MODIFIER)
            .unit(DEFAULT_UNIT)
            .isDeleted(DEFAULT_IS_DELETED);
        return productOption;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductOption createUpdatedEntity(EntityManager em) {
        ProductOption productOption = new ProductOption()
            .name(UPDATED_NAME)
            .priceModifier(UPDATED_PRICE_MODIFIER)
            .unit(UPDATED_UNIT)
            .isDeleted(UPDATED_IS_DELETED);
        return productOption;
    }

    @BeforeEach
    public void initTest() {
        productOption = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedProductOption != null) {
            productOptionRepository.delete(insertedProductOption);
            insertedProductOption = null;
        }
    }

    @Test
    @Transactional
    void createProductOption() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the ProductOption
        var returnedProductOption = om.readValue(
            restProductOptionMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(productOption)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            ProductOption.class
        );

        // Validate the ProductOption in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        assertProductOptionUpdatableFieldsEquals(returnedProductOption, getPersistedProductOption(returnedProductOption));

        insertedProductOption = returnedProductOption;
    }

    @Test
    @Transactional
    void createProductOptionWithExistingId() throws Exception {
        // Create the ProductOption with an existing ID
        productOption.setId(1L);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductOptionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(productOption)))
            .andExpect(status().isBadRequest());

        // Validate the ProductOption in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        productOption.setName(null);

        // Create the ProductOption, which fails.

        restProductOptionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(productOption)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPriceModifierIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        productOption.setPriceModifier(null);

        // Create the ProductOption, which fails.

        restProductOptionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(productOption)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUnitIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        productOption.setUnit(null);

        // Create the ProductOption, which fails.

        restProductOptionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(productOption)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllProductOptions() throws Exception {
        // Initialize the database
        insertedProductOption = productOptionRepository.saveAndFlush(productOption);

        // Get all the productOptionList
        restProductOptionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productOption.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].priceModifier").value(hasItem(DEFAULT_PRICE_MODIFIER.doubleValue())))
            .andExpect(jsonPath("$.[*].unit").value(hasItem(DEFAULT_UNIT)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    void getProductOption() throws Exception {
        // Initialize the database
        insertedProductOption = productOptionRepository.saveAndFlush(productOption);

        // Get the productOption
        restProductOptionMockMvc
            .perform(get(ENTITY_API_URL_ID, productOption.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(productOption.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.priceModifier").value(DEFAULT_PRICE_MODIFIER.doubleValue()))
            .andExpect(jsonPath("$.unit").value(DEFAULT_UNIT))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingProductOption() throws Exception {
        // Get the productOption
        restProductOptionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingProductOption() throws Exception {
        // Initialize the database
        insertedProductOption = productOptionRepository.saveAndFlush(productOption);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the productOption
        ProductOption updatedProductOption = productOptionRepository.findById(productOption.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedProductOption are not directly saved in db
        em.detach(updatedProductOption);
        updatedProductOption.name(UPDATED_NAME).priceModifier(UPDATED_PRICE_MODIFIER).unit(UPDATED_UNIT).isDeleted(UPDATED_IS_DELETED);

        restProductOptionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedProductOption.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(updatedProductOption))
            )
            .andExpect(status().isOk());

        // Validate the ProductOption in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedProductOptionToMatchAllProperties(updatedProductOption);
    }

    @Test
    @Transactional
    void putNonExistingProductOption() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        productOption.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductOptionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, productOption.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(productOption))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductOption in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchProductOption() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        productOption.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductOptionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(productOption))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductOption in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamProductOption() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        productOption.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductOptionMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(productOption)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ProductOption in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateProductOptionWithPatch() throws Exception {
        // Initialize the database
        insertedProductOption = productOptionRepository.saveAndFlush(productOption);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the productOption using partial update
        ProductOption partialUpdatedProductOption = new ProductOption();
        partialUpdatedProductOption.setId(productOption.getId());

        partialUpdatedProductOption.name(UPDATED_NAME);

        restProductOptionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProductOption.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedProductOption))
            )
            .andExpect(status().isOk());

        // Validate the ProductOption in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertProductOptionUpdatableFieldsEquals(
            createUpdateProxyForBean(partialUpdatedProductOption, productOption),
            getPersistedProductOption(productOption)
        );
    }

    @Test
    @Transactional
    void fullUpdateProductOptionWithPatch() throws Exception {
        // Initialize the database
        insertedProductOption = productOptionRepository.saveAndFlush(productOption);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the productOption using partial update
        ProductOption partialUpdatedProductOption = new ProductOption();
        partialUpdatedProductOption.setId(productOption.getId());

        partialUpdatedProductOption
            .name(UPDATED_NAME)
            .priceModifier(UPDATED_PRICE_MODIFIER)
            .unit(UPDATED_UNIT)
            .isDeleted(UPDATED_IS_DELETED);

        restProductOptionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProductOption.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedProductOption))
            )
            .andExpect(status().isOk());

        // Validate the ProductOption in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertProductOptionUpdatableFieldsEquals(partialUpdatedProductOption, getPersistedProductOption(partialUpdatedProductOption));
    }

    @Test
    @Transactional
    void patchNonExistingProductOption() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        productOption.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductOptionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, productOption.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(productOption))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductOption in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchProductOption() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        productOption.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductOptionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(productOption))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductOption in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamProductOption() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        productOption.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductOptionMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(productOption)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ProductOption in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteProductOption() throws Exception {
        // Initialize the database
        insertedProductOption = productOptionRepository.saveAndFlush(productOption);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the productOption
        restProductOptionMockMvc
            .perform(delete(ENTITY_API_URL_ID, productOption.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return productOptionRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected ProductOption getPersistedProductOption(ProductOption productOption) {
        return productOptionRepository.findById(productOption.getId()).orElseThrow();
    }

    protected void assertPersistedProductOptionToMatchAllProperties(ProductOption expectedProductOption) {
        assertProductOptionAllPropertiesEquals(expectedProductOption, getPersistedProductOption(expectedProductOption));
    }

    protected void assertPersistedProductOptionToMatchUpdatableProperties(ProductOption expectedProductOption) {
        assertProductOptionAllUpdatablePropertiesEquals(expectedProductOption, getPersistedProductOption(expectedProductOption));
    }
}
