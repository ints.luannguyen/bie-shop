package com.ints.bieshop.web.rest;

import static com.ints.bieshop.domain.InventoryTransactionsAsserts.*;
import static com.ints.bieshop.web.rest.TestUtil.createUpdateProxyForBean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ints.bieshop.IntegrationTest;
import com.ints.bieshop.domain.InventoryTransactions;
import com.ints.bieshop.repository.InventoryTransactionsRepository;
import com.ints.bieshop.repository.UserRepository;
import jakarta.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link InventoryTransactionsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class InventoryTransactionsResourceIT {

    private static final Integer DEFAULT_QUANTITY = 1;
    private static final Integer UPDATED_QUANTITY = 2;

    private static final Float DEFAULT_UNIT_PRICE = 1F;
    private static final Float UPDATED_UNIT_PRICE = 2F;

    private static final Instant DEFAULT_TRANSACTION_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TRANSACTION_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_DELETED = false;
    private static final Boolean UPDATED_IS_DELETED = true;

    private static final String ENTITY_API_URL = "/api/inventory-transactions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private InventoryTransactionsRepository inventoryTransactionsRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restInventoryTransactionsMockMvc;

    private InventoryTransactions inventoryTransactions;

    private InventoryTransactions insertedInventoryTransactions;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InventoryTransactions createEntity(EntityManager em) {
        InventoryTransactions inventoryTransactions = new InventoryTransactions()
            .quantity(DEFAULT_QUANTITY)
            .unitPrice(DEFAULT_UNIT_PRICE)
            .transactionDate(DEFAULT_TRANSACTION_DATE)
            .note(DEFAULT_NOTE)
            .isDeleted(DEFAULT_IS_DELETED);
        return inventoryTransactions;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InventoryTransactions createUpdatedEntity(EntityManager em) {
        InventoryTransactions inventoryTransactions = new InventoryTransactions()
            .quantity(UPDATED_QUANTITY)
            .unitPrice(UPDATED_UNIT_PRICE)
            .transactionDate(UPDATED_TRANSACTION_DATE)
            .note(UPDATED_NOTE)
            .isDeleted(UPDATED_IS_DELETED);
        return inventoryTransactions;
    }

    @BeforeEach
    public void initTest() {
        inventoryTransactions = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedInventoryTransactions != null) {
            inventoryTransactionsRepository.delete(insertedInventoryTransactions);
            insertedInventoryTransactions = null;
        }
    }

    @Test
    @Transactional
    void createInventoryTransactions() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the InventoryTransactions
        var returnedInventoryTransactions = om.readValue(
            restInventoryTransactionsMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(inventoryTransactions)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            InventoryTransactions.class
        );

        // Validate the InventoryTransactions in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        assertInventoryTransactionsUpdatableFieldsEquals(
            returnedInventoryTransactions,
            getPersistedInventoryTransactions(returnedInventoryTransactions)
        );

        insertedInventoryTransactions = returnedInventoryTransactions;
    }

    @Test
    @Transactional
    void createInventoryTransactionsWithExistingId() throws Exception {
        // Create the InventoryTransactions with an existing ID
        inventoryTransactions.setId(1L);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restInventoryTransactionsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(inventoryTransactions)))
            .andExpect(status().isBadRequest());

        // Validate the InventoryTransactions in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllInventoryTransactions() throws Exception {
        // Initialize the database
        insertedInventoryTransactions = inventoryTransactionsRepository.saveAndFlush(inventoryTransactions);

        // Get all the inventoryTransactionsList
        restInventoryTransactionsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inventoryTransactions.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
            .andExpect(jsonPath("$.[*].unitPrice").value(hasItem(DEFAULT_UNIT_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].transactionDate").value(hasItem(DEFAULT_TRANSACTION_DATE.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    void getInventoryTransactions() throws Exception {
        // Initialize the database
        insertedInventoryTransactions = inventoryTransactionsRepository.saveAndFlush(inventoryTransactions);

        // Get the inventoryTransactions
        restInventoryTransactionsMockMvc
            .perform(get(ENTITY_API_URL_ID, inventoryTransactions.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(inventoryTransactions.getId().intValue()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY))
            .andExpect(jsonPath("$.unitPrice").value(DEFAULT_UNIT_PRICE.doubleValue()))
            .andExpect(jsonPath("$.transactionDate").value(DEFAULT_TRANSACTION_DATE.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingInventoryTransactions() throws Exception {
        // Get the inventoryTransactions
        restInventoryTransactionsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingInventoryTransactions() throws Exception {
        // Initialize the database
        insertedInventoryTransactions = inventoryTransactionsRepository.saveAndFlush(inventoryTransactions);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the inventoryTransactions
        InventoryTransactions updatedInventoryTransactions = inventoryTransactionsRepository
            .findById(inventoryTransactions.getId())
            .orElseThrow();
        // Disconnect from session so that the updates on updatedInventoryTransactions are not directly saved in db
        em.detach(updatedInventoryTransactions);
        updatedInventoryTransactions
            .quantity(UPDATED_QUANTITY)
            .unitPrice(UPDATED_UNIT_PRICE)
            .transactionDate(UPDATED_TRANSACTION_DATE)
            .note(UPDATED_NOTE)
            .isDeleted(UPDATED_IS_DELETED);

        restInventoryTransactionsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedInventoryTransactions.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(updatedInventoryTransactions))
            )
            .andExpect(status().isOk());

        // Validate the InventoryTransactions in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedInventoryTransactionsToMatchAllProperties(updatedInventoryTransactions);
    }

    @Test
    @Transactional
    void putNonExistingInventoryTransactions() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        inventoryTransactions.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInventoryTransactionsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, inventoryTransactions.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(inventoryTransactions))
            )
            .andExpect(status().isBadRequest());

        // Validate the InventoryTransactions in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchInventoryTransactions() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        inventoryTransactions.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInventoryTransactionsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(inventoryTransactions))
            )
            .andExpect(status().isBadRequest());

        // Validate the InventoryTransactions in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamInventoryTransactions() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        inventoryTransactions.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInventoryTransactionsMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(inventoryTransactions)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the InventoryTransactions in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateInventoryTransactionsWithPatch() throws Exception {
        // Initialize the database
        insertedInventoryTransactions = inventoryTransactionsRepository.saveAndFlush(inventoryTransactions);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the inventoryTransactions using partial update
        InventoryTransactions partialUpdatedInventoryTransactions = new InventoryTransactions();
        partialUpdatedInventoryTransactions.setId(inventoryTransactions.getId());

        partialUpdatedInventoryTransactions.transactionDate(UPDATED_TRANSACTION_DATE).note(UPDATED_NOTE).isDeleted(UPDATED_IS_DELETED);

        restInventoryTransactionsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedInventoryTransactions.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedInventoryTransactions))
            )
            .andExpect(status().isOk());

        // Validate the InventoryTransactions in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertInventoryTransactionsUpdatableFieldsEquals(
            createUpdateProxyForBean(partialUpdatedInventoryTransactions, inventoryTransactions),
            getPersistedInventoryTransactions(inventoryTransactions)
        );
    }

    @Test
    @Transactional
    void fullUpdateInventoryTransactionsWithPatch() throws Exception {
        // Initialize the database
        insertedInventoryTransactions = inventoryTransactionsRepository.saveAndFlush(inventoryTransactions);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the inventoryTransactions using partial update
        InventoryTransactions partialUpdatedInventoryTransactions = new InventoryTransactions();
        partialUpdatedInventoryTransactions.setId(inventoryTransactions.getId());

        partialUpdatedInventoryTransactions
            .quantity(UPDATED_QUANTITY)
            .unitPrice(UPDATED_UNIT_PRICE)
            .transactionDate(UPDATED_TRANSACTION_DATE)
            .note(UPDATED_NOTE)
            .isDeleted(UPDATED_IS_DELETED);

        restInventoryTransactionsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedInventoryTransactions.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedInventoryTransactions))
            )
            .andExpect(status().isOk());

        // Validate the InventoryTransactions in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertInventoryTransactionsUpdatableFieldsEquals(
            partialUpdatedInventoryTransactions,
            getPersistedInventoryTransactions(partialUpdatedInventoryTransactions)
        );
    }

    @Test
    @Transactional
    void patchNonExistingInventoryTransactions() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        inventoryTransactions.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInventoryTransactionsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, inventoryTransactions.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(inventoryTransactions))
            )
            .andExpect(status().isBadRequest());

        // Validate the InventoryTransactions in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchInventoryTransactions() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        inventoryTransactions.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInventoryTransactionsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(inventoryTransactions))
            )
            .andExpect(status().isBadRequest());

        // Validate the InventoryTransactions in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamInventoryTransactions() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        inventoryTransactions.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInventoryTransactionsMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(inventoryTransactions)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the InventoryTransactions in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteInventoryTransactions() throws Exception {
        // Initialize the database
        insertedInventoryTransactions = inventoryTransactionsRepository.saveAndFlush(inventoryTransactions);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the inventoryTransactions
        restInventoryTransactionsMockMvc
            .perform(delete(ENTITY_API_URL_ID, inventoryTransactions.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return inventoryTransactionsRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected InventoryTransactions getPersistedInventoryTransactions(InventoryTransactions inventoryTransactions) {
        return inventoryTransactionsRepository.findById(inventoryTransactions.getId()).orElseThrow();
    }

    protected void assertPersistedInventoryTransactionsToMatchAllProperties(InventoryTransactions expectedInventoryTransactions) {
        assertInventoryTransactionsAllPropertiesEquals(
            expectedInventoryTransactions,
            getPersistedInventoryTransactions(expectedInventoryTransactions)
        );
    }

    protected void assertPersistedInventoryTransactionsToMatchUpdatableProperties(InventoryTransactions expectedInventoryTransactions) {
        assertInventoryTransactionsAllUpdatablePropertiesEquals(
            expectedInventoryTransactions,
            getPersistedInventoryTransactions(expectedInventoryTransactions)
        );
    }
}
