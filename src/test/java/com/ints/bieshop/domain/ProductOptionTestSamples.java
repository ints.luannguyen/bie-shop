package com.ints.bieshop.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class ProductOptionTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static ProductOption getProductOptionSample1() {
        return new ProductOption().id(1L).name("name1").unit("unit1");
    }

    public static ProductOption getProductOptionSample2() {
        return new ProductOption().id(2L).name("name2").unit("unit2");
    }

    public static ProductOption getProductOptionRandomSampleGenerator() {
        return new ProductOption().id(longCount.incrementAndGet()).name(UUID.randomUUID().toString()).unit(UUID.randomUUID().toString());
    }
}
