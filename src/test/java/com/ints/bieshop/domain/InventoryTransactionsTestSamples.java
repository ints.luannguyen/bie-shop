package com.ints.bieshop.domain;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class InventoryTransactionsTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static InventoryTransactions getInventoryTransactionsSample1() {
        return new InventoryTransactions().id(1L).quantity(1);
    }

    public static InventoryTransactions getInventoryTransactionsSample2() {
        return new InventoryTransactions().id(2L).quantity(2);
    }

    public static InventoryTransactions getInventoryTransactionsRandomSampleGenerator() {
        return new InventoryTransactions().id(longCount.incrementAndGet()).quantity(intCount.incrementAndGet());
    }
}
