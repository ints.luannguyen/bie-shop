package com.ints.bieshop.domain;

import static com.ints.bieshop.domain.DiscountTestSamples.*;
import static com.ints.bieshop.domain.ProductTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.ints.bieshop.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class DiscountTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Discount.class);
        Discount discount1 = getDiscountSample1();
        Discount discount2 = new Discount();
        assertThat(discount1).isNotEqualTo(discount2);

        discount2.setId(discount1.getId());
        assertThat(discount1).isEqualTo(discount2);

        discount2 = getDiscountSample2();
        assertThat(discount1).isNotEqualTo(discount2);
    }

    @Test
    void productTest() {
        Discount discount = getDiscountRandomSampleGenerator();
        Product productBack = getProductRandomSampleGenerator();

        discount.addProduct(productBack);
        assertThat(discount.getProducts()).containsOnly(productBack);
        assertThat(productBack.getDiscount()).isEqualTo(discount);

        discount.removeProduct(productBack);
        assertThat(discount.getProducts()).doesNotContain(productBack);
        assertThat(productBack.getDiscount()).isNull();

        discount.products(new HashSet<>(Set.of(productBack)));
        assertThat(discount.getProducts()).containsOnly(productBack);
        assertThat(productBack.getDiscount()).isEqualTo(discount);

        discount.setProducts(new HashSet<>());
        assertThat(discount.getProducts()).doesNotContain(productBack);
        assertThat(productBack.getDiscount()).isNull();
    }
}
