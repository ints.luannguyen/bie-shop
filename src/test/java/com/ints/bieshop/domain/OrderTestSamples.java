package com.ints.bieshop.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class OrderTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static Order getOrderSample1() {
        return new Order().id(1L).address("address1").phoneNumber("phoneNumber1").name("name1");
    }

    public static Order getOrderSample2() {
        return new Order().id(2L).address("address2").phoneNumber("phoneNumber2").name("name2");
    }

    public static Order getOrderRandomSampleGenerator() {
        return new Order()
            .id(longCount.incrementAndGet())
            .address(UUID.randomUUID().toString())
            .phoneNumber(UUID.randomUUID().toString())
            .name(UUID.randomUUID().toString());
    }
}
