package com.ints.bieshop.domain;

import static com.ints.bieshop.domain.OrderDetailTestSamples.*;
import static com.ints.bieshop.domain.OrderTestSamples.*;
import static com.ints.bieshop.domain.PaymentTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.ints.bieshop.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class OrderTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Order.class);
        Order order1 = getOrderSample1();
        Order order2 = new Order();
        assertThat(order1).isNotEqualTo(order2);

        order2.setId(order1.getId());
        assertThat(order1).isEqualTo(order2);

        order2 = getOrderSample2();
        assertThat(order1).isNotEqualTo(order2);
    }

    @Test
    void orderDetailTest() {
        Order order = getOrderRandomSampleGenerator();
        OrderDetail orderDetailBack = getOrderDetailRandomSampleGenerator();

        order.addOrderDetail(orderDetailBack);
        assertThat(order.getOrderDetails()).containsOnly(orderDetailBack);
        assertThat(orderDetailBack.getOrder()).isEqualTo(order);

        order.removeOrderDetail(orderDetailBack);
        assertThat(order.getOrderDetails()).doesNotContain(orderDetailBack);
        assertThat(orderDetailBack.getOrder()).isNull();

        order.orderDetails(new HashSet<>(Set.of(orderDetailBack)));
        assertThat(order.getOrderDetails()).containsOnly(orderDetailBack);
        assertThat(orderDetailBack.getOrder()).isEqualTo(order);

        order.setOrderDetails(new HashSet<>());
        assertThat(order.getOrderDetails()).doesNotContain(orderDetailBack);
        assertThat(orderDetailBack.getOrder()).isNull();
    }

    @Test
    void paymentTest() {
        Order order = getOrderRandomSampleGenerator();
        Payment paymentBack = getPaymentRandomSampleGenerator();

        order.setPayment(paymentBack);
        assertThat(order.getPayment()).isEqualTo(paymentBack);
        assertThat(paymentBack.getOrder()).isEqualTo(order);

        order.payment(null);
        assertThat(order.getPayment()).isNull();
        assertThat(paymentBack.getOrder()).isNull();
    }
}
