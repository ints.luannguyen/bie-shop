package com.ints.bieshop.domain;

import static com.ints.bieshop.domain.InventoryTransactionsTestSamples.*;
import static com.ints.bieshop.domain.ProductTestSamples.*;
import static com.ints.bieshop.domain.SupplierTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.ints.bieshop.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class InventoryTransactionsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InventoryTransactions.class);
        InventoryTransactions inventoryTransactions1 = getInventoryTransactionsSample1();
        InventoryTransactions inventoryTransactions2 = new InventoryTransactions();
        assertThat(inventoryTransactions1).isNotEqualTo(inventoryTransactions2);

        inventoryTransactions2.setId(inventoryTransactions1.getId());
        assertThat(inventoryTransactions1).isEqualTo(inventoryTransactions2);

        inventoryTransactions2 = getInventoryTransactionsSample2();
        assertThat(inventoryTransactions1).isNotEqualTo(inventoryTransactions2);
    }

    @Test
    void supplierTest() {
        InventoryTransactions inventoryTransactions = getInventoryTransactionsRandomSampleGenerator();
        Supplier supplierBack = getSupplierRandomSampleGenerator();

        inventoryTransactions.setSupplier(supplierBack);
        assertThat(inventoryTransactions.getSupplier()).isEqualTo(supplierBack);

        inventoryTransactions.supplier(null);
        assertThat(inventoryTransactions.getSupplier()).isNull();
    }

    @Test
    void productTest() {
        InventoryTransactions inventoryTransactions = getInventoryTransactionsRandomSampleGenerator();
        Product productBack = getProductRandomSampleGenerator();

        inventoryTransactions.setProduct(productBack);
        assertThat(inventoryTransactions.getProduct()).isEqualTo(productBack);

        inventoryTransactions.product(null);
        assertThat(inventoryTransactions.getProduct()).isNull();
    }
}
