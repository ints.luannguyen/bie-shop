package com.ints.bieshop.domain;

import static com.ints.bieshop.domain.InventoryTransactionsTestSamples.*;
import static com.ints.bieshop.domain.ProductTestSamples.*;
import static com.ints.bieshop.domain.SupplierTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.ints.bieshop.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class SupplierTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Supplier.class);
        Supplier supplier1 = getSupplierSample1();
        Supplier supplier2 = new Supplier();
        assertThat(supplier1).isNotEqualTo(supplier2);

        supplier2.setId(supplier1.getId());
        assertThat(supplier1).isEqualTo(supplier2);

        supplier2 = getSupplierSample2();
        assertThat(supplier1).isNotEqualTo(supplier2);
    }

    @Test
    void productTest() {
        Supplier supplier = getSupplierRandomSampleGenerator();
        Product productBack = getProductRandomSampleGenerator();

        supplier.addProduct(productBack);
        assertThat(supplier.getProducts()).containsOnly(productBack);
        assertThat(productBack.getSupplier()).isEqualTo(supplier);

        supplier.removeProduct(productBack);
        assertThat(supplier.getProducts()).doesNotContain(productBack);
        assertThat(productBack.getSupplier()).isNull();

        supplier.products(new HashSet<>(Set.of(productBack)));
        assertThat(supplier.getProducts()).containsOnly(productBack);
        assertThat(productBack.getSupplier()).isEqualTo(supplier);

        supplier.setProducts(new HashSet<>());
        assertThat(supplier.getProducts()).doesNotContain(productBack);
        assertThat(productBack.getSupplier()).isNull();
    }

    @Test
    void inventoryTransactionsTest() {
        Supplier supplier = getSupplierRandomSampleGenerator();
        InventoryTransactions inventoryTransactionsBack = getInventoryTransactionsRandomSampleGenerator();

        supplier.addInventoryTransactions(inventoryTransactionsBack);
        assertThat(supplier.getInventoryTransactions()).containsOnly(inventoryTransactionsBack);
        assertThat(inventoryTransactionsBack.getSupplier()).isEqualTo(supplier);

        supplier.removeInventoryTransactions(inventoryTransactionsBack);
        assertThat(supplier.getInventoryTransactions()).doesNotContain(inventoryTransactionsBack);
        assertThat(inventoryTransactionsBack.getSupplier()).isNull();

        supplier.inventoryTransactions(new HashSet<>(Set.of(inventoryTransactionsBack)));
        assertThat(supplier.getInventoryTransactions()).containsOnly(inventoryTransactionsBack);
        assertThat(inventoryTransactionsBack.getSupplier()).isEqualTo(supplier);

        supplier.setInventoryTransactions(new HashSet<>());
        assertThat(supplier.getInventoryTransactions()).doesNotContain(inventoryTransactionsBack);
        assertThat(inventoryTransactionsBack.getSupplier()).isNull();
    }
}
