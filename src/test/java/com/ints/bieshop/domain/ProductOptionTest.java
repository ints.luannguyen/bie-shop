package com.ints.bieshop.domain;

import static com.ints.bieshop.domain.OrderDetailTestSamples.*;
import static com.ints.bieshop.domain.ProductOptionTestSamples.*;
import static com.ints.bieshop.domain.ProductTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.ints.bieshop.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class ProductOptionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductOption.class);
        ProductOption productOption1 = getProductOptionSample1();
        ProductOption productOption2 = new ProductOption();
        assertThat(productOption1).isNotEqualTo(productOption2);

        productOption2.setId(productOption1.getId());
        assertThat(productOption1).isEqualTo(productOption2);

        productOption2 = getProductOptionSample2();
        assertThat(productOption1).isNotEqualTo(productOption2);
    }

    @Test
    void orderDetailTest() {
        ProductOption productOption = getProductOptionRandomSampleGenerator();
        OrderDetail orderDetailBack = getOrderDetailRandomSampleGenerator();

        productOption.addOrderDetail(orderDetailBack);
        assertThat(productOption.getOrderDetails()).containsOnly(orderDetailBack);
        assertThat(orderDetailBack.getProductOption()).isEqualTo(productOption);

        productOption.removeOrderDetail(orderDetailBack);
        assertThat(productOption.getOrderDetails()).doesNotContain(orderDetailBack);
        assertThat(orderDetailBack.getProductOption()).isNull();

        productOption.orderDetails(new HashSet<>(Set.of(orderDetailBack)));
        assertThat(productOption.getOrderDetails()).containsOnly(orderDetailBack);
        assertThat(orderDetailBack.getProductOption()).isEqualTo(productOption);

        productOption.setOrderDetails(new HashSet<>());
        assertThat(productOption.getOrderDetails()).doesNotContain(orderDetailBack);
        assertThat(orderDetailBack.getProductOption()).isNull();
    }

    @Test
    void productTest() {
        ProductOption productOption = getProductOptionRandomSampleGenerator();
        Product productBack = getProductRandomSampleGenerator();

        productOption.setProduct(productBack);
        assertThat(productOption.getProduct()).isEqualTo(productBack);

        productOption.product(null);
        assertThat(productOption.getProduct()).isNull();
    }
}
