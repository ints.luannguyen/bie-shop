package com.ints.bieshop.domain;

import static com.ints.bieshop.domain.CategoryTestSamples.*;
import static com.ints.bieshop.domain.DiscountTestSamples.*;
import static com.ints.bieshop.domain.InventoryTestSamples.*;
import static com.ints.bieshop.domain.InventoryTransactionsTestSamples.*;
import static com.ints.bieshop.domain.OrderDetailTestSamples.*;
import static com.ints.bieshop.domain.ProductImageTestSamples.*;
import static com.ints.bieshop.domain.ProductOptionTestSamples.*;
import static com.ints.bieshop.domain.ProductTestSamples.*;
import static com.ints.bieshop.domain.SupplierTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.ints.bieshop.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class ProductTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Product.class);
        Product product1 = getProductSample1();
        Product product2 = new Product();
        assertThat(product1).isNotEqualTo(product2);

        product2.setId(product1.getId());
        assertThat(product1).isEqualTo(product2);

        product2 = getProductSample2();
        assertThat(product1).isNotEqualTo(product2);
    }

    @Test
    void orderDetailTest() {
        Product product = getProductRandomSampleGenerator();
        OrderDetail orderDetailBack = getOrderDetailRandomSampleGenerator();

        product.addOrderDetail(orderDetailBack);
        assertThat(product.getOrderDetails()).containsOnly(orderDetailBack);
        assertThat(orderDetailBack.getProduct()).isEqualTo(product);

        product.removeOrderDetail(orderDetailBack);
        assertThat(product.getOrderDetails()).doesNotContain(orderDetailBack);
        assertThat(orderDetailBack.getProduct()).isNull();

        product.orderDetails(new HashSet<>(Set.of(orderDetailBack)));
        assertThat(product.getOrderDetails()).containsOnly(orderDetailBack);
        assertThat(orderDetailBack.getProduct()).isEqualTo(product);

        product.setOrderDetails(new HashSet<>());
        assertThat(product.getOrderDetails()).doesNotContain(orderDetailBack);
        assertThat(orderDetailBack.getProduct()).isNull();
    }

    @Test
    void productImageTest() {
        Product product = getProductRandomSampleGenerator();
        ProductImage productImageBack = getProductImageRandomSampleGenerator();

        product.addProductImage(productImageBack);
        assertThat(product.getProductImages()).containsOnly(productImageBack);
        assertThat(productImageBack.getProduct()).isEqualTo(product);

        product.removeProductImage(productImageBack);
        assertThat(product.getProductImages()).doesNotContain(productImageBack);
        assertThat(productImageBack.getProduct()).isNull();

        product.productImages(new HashSet<>(Set.of(productImageBack)));
        assertThat(product.getProductImages()).containsOnly(productImageBack);
        assertThat(productImageBack.getProduct()).isEqualTo(product);

        product.setProductImages(new HashSet<>());
        assertThat(product.getProductImages()).doesNotContain(productImageBack);
        assertThat(productImageBack.getProduct()).isNull();
    }

    @Test
    void productOptionTest() {
        Product product = getProductRandomSampleGenerator();
        ProductOption productOptionBack = getProductOptionRandomSampleGenerator();

        product.addProductOption(productOptionBack);
        assertThat(product.getProductOptions()).containsOnly(productOptionBack);
        assertThat(productOptionBack.getProduct()).isEqualTo(product);

        product.removeProductOption(productOptionBack);
        assertThat(product.getProductOptions()).doesNotContain(productOptionBack);
        assertThat(productOptionBack.getProduct()).isNull();

        product.productOptions(new HashSet<>(Set.of(productOptionBack)));
        assertThat(product.getProductOptions()).containsOnly(productOptionBack);
        assertThat(productOptionBack.getProduct()).isEqualTo(product);

        product.setProductOptions(new HashSet<>());
        assertThat(product.getProductOptions()).doesNotContain(productOptionBack);
        assertThat(productOptionBack.getProduct()).isNull();
    }

    @Test
    void inventoryTransactionsTest() {
        Product product = getProductRandomSampleGenerator();
        InventoryTransactions inventoryTransactionsBack = getInventoryTransactionsRandomSampleGenerator();

        product.addInventoryTransactions(inventoryTransactionsBack);
        assertThat(product.getInventoryTransactions()).containsOnly(inventoryTransactionsBack);
        assertThat(inventoryTransactionsBack.getProduct()).isEqualTo(product);

        product.removeInventoryTransactions(inventoryTransactionsBack);
        assertThat(product.getInventoryTransactions()).doesNotContain(inventoryTransactionsBack);
        assertThat(inventoryTransactionsBack.getProduct()).isNull();

        product.inventoryTransactions(new HashSet<>(Set.of(inventoryTransactionsBack)));
        assertThat(product.getInventoryTransactions()).containsOnly(inventoryTransactionsBack);
        assertThat(inventoryTransactionsBack.getProduct()).isEqualTo(product);

        product.setInventoryTransactions(new HashSet<>());
        assertThat(product.getInventoryTransactions()).doesNotContain(inventoryTransactionsBack);
        assertThat(inventoryTransactionsBack.getProduct()).isNull();
    }

    @Test
    void inventoryTest() {
        Product product = getProductRandomSampleGenerator();
        Inventory inventoryBack = getInventoryRandomSampleGenerator();

        product.addInventory(inventoryBack);
        assertThat(product.getInventories()).containsOnly(inventoryBack);
        assertThat(inventoryBack.getProduct()).isEqualTo(product);

        product.removeInventory(inventoryBack);
        assertThat(product.getInventories()).doesNotContain(inventoryBack);
        assertThat(inventoryBack.getProduct()).isNull();

        product.inventories(new HashSet<>(Set.of(inventoryBack)));
        assertThat(product.getInventories()).containsOnly(inventoryBack);
        assertThat(inventoryBack.getProduct()).isEqualTo(product);

        product.setInventories(new HashSet<>());
        assertThat(product.getInventories()).doesNotContain(inventoryBack);
        assertThat(inventoryBack.getProduct()).isNull();
    }

    @Test
    void categoryTest() {
        Product product = getProductRandomSampleGenerator();
        Category categoryBack = getCategoryRandomSampleGenerator();

        product.setCategory(categoryBack);
        assertThat(product.getCategory()).isEqualTo(categoryBack);

        product.category(null);
        assertThat(product.getCategory()).isNull();
    }

    @Test
    void discountTest() {
        Product product = getProductRandomSampleGenerator();
        Discount discountBack = getDiscountRandomSampleGenerator();

        product.setDiscount(discountBack);
        assertThat(product.getDiscount()).isEqualTo(discountBack);

        product.discount(null);
        assertThat(product.getDiscount()).isNull();
    }

    @Test
    void supplierTest() {
        Product product = getProductRandomSampleGenerator();
        Supplier supplierBack = getSupplierRandomSampleGenerator();

        product.setSupplier(supplierBack);
        assertThat(product.getSupplier()).isEqualTo(supplierBack);

        product.supplier(null);
        assertThat(product.getSupplier()).isNull();
    }
}
